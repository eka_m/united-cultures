$(document).ready(() => {

    /* AOS */
    AOS.init({
        offset: 200,
        duration: 600,
        easing: 'ease-in-sine',
        delay: 100,
        once:true,
    });
    /* END AOS */


    /* MENU */
    var overlayElem = '<div class="overlay"></div>';

    $('.toggle-wrapper-menu').click(function () {
        closePressBriefly();
        toggleMainMenu();
    });

    $('.toggle-press-briefly').click(function () {
        closeMainMenu();
        togglePressBriefly();
    });

    $(document).on('click', '.overlay' ,function() {
        closeMainMenu();
        closePressBriefly();
    });

    function toggleMainMenu() {
        $('.toggle-wrapper-menu').toggleClass('open');
        $('.main-nav').toggleClass('opened');
        $('html').toggleClass('overflow');
        overlay();
    }

    function togglePressBriefly() {
        $('.toggle-press-briefly').toggleClass('open');
        $('.press-briefly').toggleClass('opened');
        $('html').toggleClass('overflow');
        overlay();
    }

    function closeMainMenu() {
        if($('.toggle-wrapper-menu').hasClass('open')) {
            $('.toggle-wrapper-menu').removeClass('open');
            $('.main-nav').removeClass('opened');
            $('html').removeClass('overflow');
            overlay('remove');
        }
    }

    function closePressBriefly() {
        if($('.toggle-press-briefly').hasClass('open')) {
            $('.toggle-press-briefly').removeClass('open');
            $('.press-briefly').removeClass('opened');
            $('html').removeClass('overflow');
            overlay('remove');
        }
    }

    function overlay(type = 'toggle') {
        if(type === 'add') {
            $('body').append(overlayElem);
        } else if(type === 'remove') {
            $('body').find('.overlay').remove();
        } else {
            if($('body').has('.overlay').length) {
                $('body').find('.overlay').remove();
            } else {
                $('body').append(overlayElem);
            }
        }
    }

    /* END MENU */

    /* UNITE GALLERY */
    if( $('#monthgallery').length) {
        $('#monthgallery').unitegallery({
            slider_control_zoom: false,
            slider_enable_zoom_panel: false,
            gallery_height:600,
            thumb_height:80,		
            thumb_width:120,		
        });
    }


    /* END UNITE GALLERY */

    /* MOUSEWHELL */
    function mousewhell () {
        $('#article-read').bind('mousewheel', function(e) {
            var padding = $('.article-read__cover__title').height();
            if(e.deltaY == 7) {
                $('.article-read__rightpanel').removeClass('show');
                $('.article-read__cover__title').removeClass('topanel');
                // $('body').toggleClass('overflow');
            } else if (e.deltaY < 0){
              
                $('.article-read__rightpanel').addClass('show')
                .find('.article-read__rightpanel__content')
                .css({'transform': 'translateY('+padding+'px)'});
                
                $('.article-read__cover__title').addClass('topanel');
                // $('body').toggleClass('overflow');
            }
        });
    }
    if ($('window').with > 768) {
mousewhell();
    }
$(window).on('resize', function(){
    var win = $(this); 
    if (win.width() > 768) {
        mousewhell();
    }
});
    /* END MOUSEWHELL */

});