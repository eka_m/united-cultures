$(document).ready(function() {
	var slider = $('.main-slider');
	var latestArticles = $('.latest-articles');
	slider.slick({ 
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		prevArrow: '<a href="javascript:void(0)" class="slick-nav slick-nav--prev"><img src="/images/left-chevron.svg"></a>',
		nextArrow: '<a href="javascript:void(0)" class="slick-nav  slick-nav--next"><img src="/images/right-chevron.svg"></a>',
		responsive: [
			{
			  breakpoint: 480,
			  settings: {
				arrows: false
			}}
		]
	});
	slider.slickAnimation();

	latestArticles.slick({ 
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 3,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 5000,
		prevArrow: '<a href="javascript:void(0)" class="slick-nav slick-nav--prev"><img src="/images/left-chevron.svg"></a>',
		nextArrow: '<a href="javascript:void(0)" class="slick-nav  slick-nav--next"><img src="/images/right-chevron.svg"></a>',
		responsive: [
			{
			  breakpoint: 960,
			  settings: {
				arrows: false,
				slidesToShow: 3,
				slidesToScroll:3,
			}},
			{
			  breakpoint: 768,
			  settings: {
				arrows: false,
				slidesToShow: 2,
				slidesToScroll:2,
			}},
			{
			  breakpoint: 480,
			  settings: {
				arrows: false,
				slidesToShow: 1,
				slidesToScroll:1,
			}}
		]
	});

});