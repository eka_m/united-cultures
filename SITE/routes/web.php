<?php


use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

Route::group(['prefix' => LaravelLocalization::setLocale()], function () {
    Route::get('/', 'HomeController@show');
    Route::get('/home', 'HomeController@show')->name('home');
    Route::get('/page/{url}', 'PagesController@show')->name('page');
    Route::get('/reports/{slug}', 'ProductsController@show')->name('reports');

    Route::get('/article/{url}', 'ArticlesController@show')->name('article');
    Route::get('/article/category/{id}', 'ArticlesController@byCategory');



});

Route::get('/home/search', 'HomeController@search')->name('search');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::post('/contactform', 'FormsController@contactForm')->name('contactform');

/* AUTH ROUTES */
Auth::routes();
/* END AUTH ROUTES */

/* ADMIN ROUTES _________________________________________________________________________________________ */
Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {
        Route::get('/', 'Admin\HomeController@show');
    /* PAGES ___________________________________________________________________________________________*/
        Route::post('/pages/sort', 'Admin\PagesController@sort');
        Route::resource('/pages', 'Admin\PagesController');
    /* END PAGES ___________________________________________________________________________________________*/
    /* CATEGORIES ___________________________________________________________________________________________*/
        // Route::post('/categories/sort', 'Admin\CategoriesController@sort');
        // Route::resource('/categories', 'Admin\CategoriesController');
    /* END CATEGORIES ___________________________________________________________________________________________*/
    /* ARTICLES ___________________________________________________________________________________________*/
        Route::get('/articles/search', 'Admin\ArticlesController@search')->name('articles.search');
        Route::get('/articles/searcharticle', 'Admin\ArticlesController@searchArticle');
        Route::get('/articles/setnext/{id}', 'Admin\ArticlesController@setnext')->name('articles.setnext');
        Route::post('/articles/searchparent', 'Admin\ArticlesController@searchparent');
        Route::get('/articles/setstatus/{id}', 'Admin\ArticlesController@setStatus')->name('articles.setstatus');
        Route::get('/articles/setinteresting/{id}', 'Admin\ArticlesController@setInteresting')->name('articles.setinteresting');
        Route::get('/articles/setpress/{id}', 'Admin\ArticlesController@setPress')->name('articles.setpress');
        Route::post('/articles/sort/{id}', 'Admin\ArticlesController@sort')->name('articles.sort');
        Route::post('/articles/fakeviews/{id}', 'Admin\ArticlesController@fakeViews')->name('articles.fakeviews');
        Route::get('/articles/popup/{id}', 'Admin\ArticlesController@popup')->name('articles.popup');
        Route::resource('/articles', 'Admin\ArticlesController');
    /* END ARTICLES ___________________________________________________________________________________________*/
    /* REPORTS ___________________________________________________________________________________________*/
        Route::get('/reports/search', 'Admin\ReportsController@search')->name('reports.search');
        Route::get('/reports/setstatus/{id}', 'Admin\ReportsController@setStatus')->name('reports.setstatus');
        Route::post('/reports/sort/{id}', 'Admin\ReportsController@sort')->name('reports.sort');
        Route::resource('/reports', 'Admin\ReportsController');
    /* END REPORTS ___________________________________________________________________________________________*/
    /* BRIEFS ___________________________________________________________________________________________*/
        Route::get('/briefs/search', 'Admin\BriefsController@search')->name('briefs.search');
        Route::get('/briefs/setstatus/{id}', 'Admin\BriefsController@setStatus')->name('briefs.setstatus');
        Route::post('/briefs/sort/{id}', 'Admin\BriefsController@sort')->name('briefs.sort');
        Route::resource('/briefs', 'Admin\BriefsController');
    /* END BRIEFS ___________________________________________________________________________________________*/
    /* VIDEOS ___________________________________________________________________________________________*/
        Route::get('/videos/search', 'Admin\VideosController@search')->name('videos.search');
        Route::get('/videos/async', 'Admin\VideosController@async')->name('videos.async');
        Route::resource('/videos', 'Admin\VideosController');
    /* END VIDEOS ___________________________________________________________________________________________*/
    /* SLIDES ___________________________________________________________________________________________*/
        Route::get('/slides/search', 'Admin\SlidesController@search')->name('slides.search');
        Route::get('/slides/async', 'Admin\SlidesController@async')->name('slides.async');
        Route::resource('/slides', 'Admin\SlidesController');
    /* END SLIDES ___________________________________________________________________________________________*/
    /* BANNERS ___________________________________________________________________________________________*/
        // Route::get('/banners/search', 'Admin\BannersController@search')->name('banners.search');
        // Route::get('/banners/async', 'Admin\BannersController@async')->name('banners.async');
        // Route::resource('/banners', 'Admin\BannersController');
    /* END BANNERS ___________________________________________________________________________________________*/
    /* LOCALES ___________________________________________________________________________________________*/
        Route::get('/locale/setstatus/{id}', 'Admin\LocaleController@setStatus');
        Route::post('/locale/sort', 'Admin\LocaleController@sort');
        Route::resource('/locale', 'Admin\LocaleController');
    /* END LOCALES ___________________________________________________________________________________________*/
    /* USERS ___________________________________________________________________________________________*/
        Route::get('/users/delete/{id}', 'Admin\UsersController@destroy')->name('users.delete');
        Route::resource('/users', 'Admin\UsersController');
        Route::get('/mediamanager', 'Admin\MediaController@show')->name('mediamanager.index');
    /* END USERS ___________________________________________________________________________________________*/
    /* GALLERIES ___________________________________________________________________________________________*/
        Route::get('/galleries/getAll', 'Admin\GalleryController@getAllGalleriesInJson');
        Route::get('/galleries/setmonth/{id}', 'Admin\GalleryController@setMonth');
        Route::resource('/galleries', 'Admin\GalleryController');
    /* END GALLERIES ___________________________________________________________________________________________*/
    /* SETTINGS ___________________________________________________________________________________________*/
        Route::get('/settings/{name}', 'Admin\SettingController@settingsfor')->name('settings.for');
        Route::get('/settings', 'Admin\SettingController@index')->name('settings');
        Route::put('/settings/{name}', 'Admin\SettingController@update')->name('settings.update');
    /* END SETTINGS ___________________________________________________________________________________________*/

});