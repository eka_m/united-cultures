const {mix} = require('laravel-mix');

mix.browserSync({
    // open: 'external',
    proxy: 'localhost:8000',
    host: 'localhost:8000',
    port: 8080
});

mix.js('resources/assets/js/app.js', 'public/js').extract([
    'vue',
]);
mix.babel(['resources/assets/js/scripts/custom.js'], 'public/js/custom.js');
mix.sass('resources/assets/sass/app.scss', 'public/css');
mix.sass('resources/assets/sass/animates.scss', 'public/css')
.options({
    processCssUrls: false,
    postcss: [
        require('autoprefixer')
    ],
});
mix.webpackConfig({
    output: {
        publicPath: '/'
    }
});

// mix.copyDirectory('resources/assets/img', 'public/images')