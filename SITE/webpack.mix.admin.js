const {mix} = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
// mix.browserSync({
//     open: 'external',
//     proxy: 'sizinavropa.dev',
//     host: 'sizinavropa.dev',
//     port: 8080
// });

mix.js('resources/assets/admin/js/app.js', 'public/adminpanel/js').extract([
    'jquery',
    'vue',
    'axios'
]);
mix.sass('resources/assets/admin/sass/app.scss', 'public/adminpanel/css')
    .options({
        processCssUrls: false,
        postcss: [
            require('autoprefixer')
        ],
    });

mix.webpackConfig({
    output: {
        publicPath: '/'
    }
});
mix.copyDirectory('resources/assets/admin/img', 'public/adminpanel/img');
mix.copyDirectory('resources/assets/admin/fonts', 'public/adminpanel/fonts');