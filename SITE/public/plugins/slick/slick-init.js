$(document).ready(function() {
	var slider = $('.main-slider');
	slider.slick({ 
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		prevArrow: '<a href="javascript:void(0)" class="slick-nav slick-nav--prev"><img src="/images/left-chevron.svg"></a>',
		nextArrow: '<a href="javascript:void(0)" class="slick-nav  slick-nav--next"><img src="/images/right-chevron.svg"></a>',
		responsive: [
			{
			  breakpoint: 480,
			  settings: {
				arrows: false
			}}
		]
	});
	slider.slickAnimation();
});