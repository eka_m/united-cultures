<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('page_id')->default(0);
            $table->tinyInteger('category_id')->default(0);
            $table->text('name');
            $table->string('slug');
            $table->longtext('content')->nullable();
            $table->string('short')->nullable();
            $table->text('images')->nullable();
            $table->integer('pos')->default(9999);
            $table->text('params')->nullable();
            $table->text('keywords')->nullable();
            $table->text('description')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
