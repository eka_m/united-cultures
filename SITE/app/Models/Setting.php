<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $casts = [
        'content' => 'array',
    ];

    // public function getContentAttribute($value) {
    //     return collect(json_decode($value,true));
    // }

    public static function take($name) {
        return self::where(['name' => $name])->firstOrFail();
    }
}
