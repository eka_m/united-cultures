<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $casts = [
        'name' => 'array',
        'params' => 'array'
    ];
}
