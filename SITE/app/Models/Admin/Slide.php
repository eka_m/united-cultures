<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $fillable = ['name', 'description', 'link', 'image', 'pos', 'status'];
}
