<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';

    protected $fillable = [
        'name',
        'short',
        'content',
        'status',
        'next',
        'interesting',
        'page_id',
        'url',
        'keywords',
        'description',
        'image',
        'cover',
        'views',
        'fakeviews',
        'press',
        'coveropacity',
        'locale_id',
        'parent_id',
        'pos',
        'date',
        'venue',
        'start',
        'end',
        'event_name'
    ];

    protected $casts = [
        'name' => 'array',
        'short' => 'array',
        'keywords' => 'array',
        'description' => 'array',
        'content' => 'array',
        'params' => 'array',
        'event_name' => 'array',
    ];


    public function page()
    {
        return $this->belongsTo('App\Models\Admin\Page', 'page_id')->select(['name', 'id']);
    }

    public function locale () {
        return $this->belongsTo('App\Models\Admin\Locale');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Admin\Article', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Admin\Article', 'parent_id', 'id');
    }

}
