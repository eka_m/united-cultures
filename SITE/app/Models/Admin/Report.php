<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table = 'reports';
    protected $fillable = [
        'name',
        'year',
        'status',
        'slug',
        'file',
        'image',
        'params',
        'pos'
    ];

    protected $casts = [
        'name' => 'array',
        'params' => 'array',
    ];
}
