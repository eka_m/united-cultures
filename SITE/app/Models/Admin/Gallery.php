<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $fillable = ['name','description','type','params','images'];

    protected $casts = [
        'params' => 'array'
    ];
}
