<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Brief extends Model
{
    protected $table = 'briefs';
    protected $fillable = [
        'name',
        'year',
        'status',
        'slug',
        'file',
        'image',
        'params',
        'pos'
    ];

    protected $casts = [
        'name' => 'array',
        'params' => 'array',
    ];
}
