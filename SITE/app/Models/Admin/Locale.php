<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Locale extends Model
{
    protected $fillable = ['name', 'code', 'slug', 'pos', 'status'];

    public function articles () {
        return $this->hasMany('App\Models\Admin\Article');
    }
}
