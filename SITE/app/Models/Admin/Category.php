<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name', 'parent_id', 'slug', 'image', 'params', 'pos'];

    protected $casts = [
        'name' => 'array',
        'params' => 'array'
    ];

    public function parent()
    {
        return $this->belongsTo('App\Models\Admin\Category', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Admin\Category', 'parent_id', 'id');
    }

    public static function tree()
    {
        return static::with(implode('.', array_fill(0, 4, 'children')))->where('parent_id', '=', NULL)->get();
    }

}
