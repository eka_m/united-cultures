<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = ['name', 'content', 'parent_id', 'url', 'keywords', 'description', 'img', 'logo', 'cover', 'coveropacity', 'place', 'type', 'params'];

    protected $table = 'pages';

    protected $casts = [
        'name' => 'array',
        'short' => 'array',
        'keywords' => 'array',
        'description' => 'array',
        'content' => 'array',
        'params' => 'array'
    ];

    public function parent()
    {
        return $this->belongsTo('App\Models\Admin\Page', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Admin\Page', 'parent_id', 'id');
    }

    public static function tree()
    {
        return static::with(implode('.', array_fill(0, 4, 'children')))->where('parent_id', '=', NULL)->get();
    }

    public function gallery()
    {
        return $this->belongsTo('App\Models\Admin\Gallery');
    }
}
