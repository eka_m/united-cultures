<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['name', 'content', 'status'];

    public function getContentAttribute($value) {
            return json_decode($value);
    }

    public function setContentAttribute($value) {
        if(is_array($value)) {
            $this->attributes['content'] = json_encode($value);
        } else {
            $this->attributes['content']  = $value;
        }
    }
}
