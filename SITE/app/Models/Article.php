<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'views',
        'fakeviews'
    ];
    protected $casts = [
        'name' => 'array',
        'short' => 'array',
        'keywords' => 'array',
        'description' => 'array',
        'content' => 'array',
        'params' => 'array',
        'event_name' => 'array',
    ];

    public function page() {
        return $this->belongsTo('App\Models\Page');
    }

    public function getCreatedAtAttribute($value){
        return Carbon::parse($value)->format('Y/m/d');
    }

    public function locale () {
        return $this->belongsTo('App\Models\Locale');
    }

    public function parent() {
        return $this->belongsTo('App\Models\Article', 'parent_id');
    }

    public function children() {
        return $this->hasMany('App\Models\Article', 'parent_id', 'id');
    }

    public function next(){
        return self::where('id', '>', $this->id)->orderBy('id','asc')->first();
    }
    public  function previous(){
        return self::where('id', '<', $this->id)->orderBy('id','desc')->first();
    }
}
