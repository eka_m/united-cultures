<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brief extends Model
{
    protected $casts = [
        'name' => 'array',
        'params' => 'array',
    ];
}
