<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Article;
use App\Models\Admin\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArticlesController extends GlobalController
{
    private $roles = ['Super','Admin','Moderator'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $articles = Article::select('id', 'page_id', 'name', 'url', 'image', 'next', 'press', 'interesting', 'status', 'created_at', 'pos', 'views', 'fakeviews')
                ->with('page')
                ->orderBy('pos', 'DESC')
                ->paginate(10);
            return view('admin.pages.articles.articles', ['articles' => $articles]);
        } catch (\Exception $e) {

        }
    }

    public function search(Request $request)
    {
        try {
            $field = $request->get('searchfield');
            $articles = Article::select('id', 'page_id', 'name', 'url', 'image', 'next', 'press', 'interesting', 'status', 'created_at', 'pos', 'views', 'fakeviews')
                ->with('page')
                ->where('name', 'like', '%' . $field . '%')
                ->orderBy('pos', 'DESC')
                ->paginate(10);
            if (empty($field) || count($articles) == 0) {
                $notification = [
                    'message' => 'Nothing found',
                    'type' => 'danger'
                ];
                return redirect()->route('articles.index')->with('notification', $notification);
            }
            return view('admin.pages.articles.articles', ['articles' => $articles]);
        } catch (\Exception $e) {

        }
    }
    public function searchparent(Request $request)
    {
        try {
            $field = $request->input('searchfield');
            $id = $request->input('id');
            $articles = Article::select(['id', 'parent_id', 'name', 'locale_id'])->where('name', 'like', '%' . $field . '%')
                ->where('id', '!=', $id)
                ->with('children')
                ->orderBy('created_at', 'DESC')
                ->get();

            return response()->json(['articles' => $articles, 'message' => 'Search progress...']);
        } catch (\Exception $e) {

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // try {
            $pages = Page::all();
            $article = new Article();
            return view('admin.pages.articles.create', compact('pages','article'));
        // } catch (\Exception $e) {

        // }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $oldPos = DB::table('articles')->max('pos');
            $inputs = $request->all();
            $inputs['pos'] = $oldPos + 1;
            Article::create($inputs);
            $notification = [
                'type' => 'success',
                'message' => 'Article Saved'
            ];
            return redirect()->route('articles.index')->with('notification', $notification);
        } catch (\Exception $e) {

        }
    }

    public function setNext($id)
    {
        try {
            $old = Article::where('next', true)->where('id', '!=', $id)->first();
            if ($old) {
                $old->next = false;
                $old->save();
            }
            $article = Article::find($id);
            $article->next = !$article->next;
            $article->save();
            return response()->json(['message' => 'Next event changed']);
        } catch (\Exception $e) {
            return response()->json([], 500);
        }
    }

    public function setStatus($id)
    {
        $this->checkRole($this->roles);
        try {
            $article = Article::find($id);
            $article->status = !$article->status;
            $article->save();
            return response()->json(['message' => 'Status updated']);
        } catch (\Exception $e) {
            return response()->json([], 500);
        }
    }

    public function setInteresting($id)
    {
        $this->checkRole($this->roles);
        try {
            $article = Article::find($id);
            $article->interesting = !$article->interesting;
            $article->save();
            return response()->json(['message' => 'Updated']);
        } catch (\Exception $e) {
            return response()->json([], 500);
        }
    }

    public function setPress($id)
    {
        $this->checkRole($this->roles);
        try {
            $article = Article::find($id);
            $article->press = !$article->press;
            $article->save();
            return response()->json(['message' => 'Updated']);
        } catch (\Exception $e) {
            return response()->json([], 500);
        }
    }

    public function fakeViews(Request $request, $id)
    {
        $this->checkRole($this->roles);
        try {
            $article = Article::find($id);
            $article->update($request->all());
            return response()->json(['message' => 'Fake data setted']);
        } catch (\Exception $e) {
            return response()->json([], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article $article
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $pages = Page::all();
            $article = Article::where('id', $id)->with(['parent' => function ($query) {
                $query->select(['id', 'parent_id', 'name', 'locale_id'])->with('children');
            }])->firstOrFail();

            return view('admin.pages.articles.edit', ['article' => $article, 'pages' => $pages]);
        } catch (\Exception $e) {

        }
    }

    public function translate($id)
    {
        try {
            $pages = Page::all();
            $article = Article::where('id', $id)->firstOrFail();
            return view('admin.pages.articles.translate', ['article' => $article, 'pages' => $pages]);
        } catch (\Exception $e) {

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Article $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        try {
            $inputs = $request->all();
            $article->update($inputs);
            $notification = [
                'type' => 'success',
                'message' => 'Article Updated'
            ];
            return redirect()->route('articles.index')->with('notification', $notification);
        } catch (\Exception $e) {
        }
    }

    public function sort(Request $request, $id)
    {
        $this->checkRole($this->roles);
        try {
            $pos = $request->input('newpos');
            $currentpos = $request->input('pos');
            if ($pos === 1) {
                $oldPos = Article::where('pos', '<', $currentpos)->max('pos');
            } elseif ($pos === -1) {
                $oldPos = Article::where('pos', '>', $currentpos)->min('pos');
            }
            if ($oldPos) {
                $oldArticle = Article::where('pos', $oldPos)->first();
                $prevpos = $oldArticle->pos;
                $oldArticle->update(['pos' => $currentpos]);
                $article = Article::find($id);
                $article->update(['pos' => $prevpos]);
                return response()->json(['message' => 'Sorted'], 200);
            } else {
                return response()->json(['message' => 'Not sorted', 'type' => 'warning'], 200);
            }
        } catch (\Exception $e) {
            return response()->json([], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $this->checkRole($this->roles);
        try {
            $article->delete();
            return response()->json(['message' => 'Deleted'], 200);
        } catch (\Exception $e) {
            return response()->json([], 500);
        }

    }
}
