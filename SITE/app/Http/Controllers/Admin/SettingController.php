<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Article;
use App\Models\Admin\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\GlobalController;

class SettingController extends GlobalController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $settings = Setting::all();
        $settings = $settings->toArray();
        foreach ($settings as $key => $value) {
            $settings[$value['name']] = $value;
            unset($settings[$key]);
        }
      
        return view('admin.pages.settings.settings', compact('settings'));
    }

    public function settingsfor($name)
    {
        $settings = Setting::where('name', $name)->firstOrFail();
        return view('admin.pages.settings.'.$name, compact('settings'));
    }


    public function update(Request $request, $name)
    {
        try {
            $content = $request->input('content');
            $setting = Setting::where('name', $name)->first();
            $setting->content = $content;
            $setting->save();
            $notification = [
                'type' => 'success',
                'message' => 'Changes saved'
            ];
            return redirect()->route('settings.for', $name)->with(compact('notification'));
        } catch (\Exception $e) {
            $notification = [
                'type' => 'error',
                'message' => 'Oooops... Sorry something went wrong'
            ];
            return redirect()->route('settings.for', $name)->with(compact('notification'));
        }
    }
}
