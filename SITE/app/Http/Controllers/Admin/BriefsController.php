<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Admin\Brief;
use Illuminate\Support\Facades\DB;
class BriefsController extends GlobalController
{
    private $roles = ['Super','Admin','Moderator'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $briefs = Brief::orderBy('pos', 'DESC')->paginate(10);
            return view('admin.pages.briefs.briefs', compact('briefs'));
        } catch (\Exception $e) {

        }
    }

    public function search(Request $request)
    {
        try {
            $field = $request->get('searchfield');
            $briefs = Brief::where('name', 'like', '%' . $field . '%')->orderBy('pos', 'DESC')->paginate(10);
            if (empty($field) || count($briefs) == 0) {
                $notification = [
                    'message' => 'Nothing found',
                    'type' => 'danger'
                ];
                return redirect()->route('briefs.index')->with('notification', $notification);
            }
            return view('admin.pages.briefs.briefs', compact('briefs'));
        } catch (\Exception $e) {

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $brief = new Brief();
            return view('admin.pages.briefs.create', compact('brief'));
        } catch (\Exception $e) {

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $oldPos = DB::table('briefs')->max('pos');
            $inputs = $request->all();
            $inputs['pos'] = $oldPos + 1;
            Brief::create($inputs);
            $notification = [
                'type' => 'success',
                'message' => 'Brief Saved'
            ];
            return redirect()->route('briefs.index')->with('notification', $notification);
        } catch (\Exception $e) {

        }
    }

    public function setStatus($id)
    {
        $this->checkRole($this->roles);
        try {
            $brief = Brief::find($id);
            $brief->status = !$brief->status;
            $brief->save();
            return response()->json(['message' => 'Status updated']);
        } catch (\Exception $e) {
            return response()->json([], 500);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Brief $brief
     * @return \Illuminate\Http\Response
     */
    public function show(Brief $brief)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Brief $brief
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $brief = Brief::find($id);
            return view('admin.pages.briefs.edit', compact('brief'));
        } catch (\Exception $e) {

        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Brief $brief
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brief $brief)
    {
        try {
            $inputs = $request->all();
            $brief->update($inputs);
            $notification = [
                'type' => 'success',
                'message' => 'Brief Updated'
            ];
            return redirect()->route('briefs.index')->with('notification', $notification);
        } catch (\Exception $e) {
        }
    }

    public function sort(Request $request, $id)
    {
        $this->checkRole($this->roles);
        try {
            $pos = $request->input('newpos');
            $currentpos = $request->input('pos');
            if ($pos === 1) {
                $oldPos = Brief::where('pos', '<', $currentpos)->max('pos');
            } elseif ($pos === -1) {
                $oldPos = Brief::where('pos', '>', $currentpos)->min('pos');
            }
            if ($oldPos) {
                $oldArticle = Brief::where('pos', $oldPos)->first();
                $prevpos = $oldArticle->pos;
                $oldArticle->update(['pos' => $currentpos]);
                $brief = Brief::find($id);
                $brief->update(['pos' => $prevpos]);
                return response()->json(['message' => 'Sorted'], 200);
            } else {
                return response()->json(['message' => 'Not sorted', 'type' => 'warning'], 200);
            }
        } catch (\Exception $e) {
            return response()->json([], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brief $brief
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brief $brief)
    {
        $this->checkRole($this->roles);
        try {
            $brief->delete();
            return response()->json(['message' => 'Deleted'], 200);
        } catch (\Exception $e) {
            return response()->json([], 500);
        }

    }
}
