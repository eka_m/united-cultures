<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\GlobalController;
class MediaController extends GlobalController
{
    public function show()
    {
        return view('admin.pages.media.media');
    }
}
