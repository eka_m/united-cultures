<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin\Category;

class CategoriesController extends GlobalController
{
    private $roles = ['Super','Admin','Moderator'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->checkRole($this->roles);
        // try {
            $categories = Category::orderBy('pos', 'ASC')->get();
            $categories = self::mapTree($categories);
            return view('admin.pages.categories.categories', ['categories' => $categories]);
        // } catch (\Exception $e) {
        // }
    }

    private static function mapTree($dataset, $parent = 0)
    {
        $tree = array();
        foreach ($dataset as $id => $node) {
            if ($node->parent_id != $parent) continue;
            $node->children = self::mapTree($dataset, $node->id);
            $tree[$id] = $node;
        }
        return $tree;
    }


    public function sort(Request $request)
    {
        $data = $request->all();
        foreach ($data as $value) {
            $category = Category::find($value['id']);
            $category->pos = $value['order'];
            $category->parent_id = isset($value['parentId']) ? $value['parentId'] : 0;
            $category->save();
        }
        return response()->json(['message' => 'Сортировка выполнена'], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->checkRole($this->roles);
        try {
            $categories = Category::all();
            $category = new Category();
            return view('admin.pages.categories.create', compact('category','categories'));
        } catch (\Exception $e) {

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->checkRole($this->roles);
        // try {
            $inputs = $request->all();
            Category::create($inputs);
            $notification = [
                'type' => 'success',
                'message' => 'Category Saved'
            ];
            return redirect()->route('categories.index')->with('notification', $notification);
        // } catch (\Exception $e) {

        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->checkRole($this->roles);
        $category = Category::where('id', $id)->with('parent')->firstOrFail();
        $categories = Category::all();
        return view('admin.pages.categories.edit', compact('category','categories'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $this->checkRole($this->roles);
        try {
            $inputs = $request->all();
            $category->update($inputs);
            $notification = [
                'type' => 'success',
                'message' => 'Category Updated'
            ];
            return redirect()->route('categories.index')->with('notification', $notification);
        } catch (\Exception $e) {
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->checkRole($this->roles);
        try {
            $category = Category::where('id', '=', $id)->with('children')->first();
            $children = $category->children;
            $category->delete();
            if (count($children) > 0) {
                foreach ($children as $child) {
                    $this->destroy($child['id']);
                }
            }
            return response()->json(['message' => 'Deleted'], 200);
        } catch (\Exception $e) {
            return response()->json([], 500);
        }
    }
}
