<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Roles;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class UsersController extends GlobalController
{
    private $roles = ['Super','Admin'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->checkRole($this->roles);
        $users = User::orderBy('created_at','DESC')->where('role_id', '!=', '1')->with('role')->get();
        return view('admin.pages.users.users', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->checkRole($this->roles);
        $roles = Roles::where('name', '!=', 'Super')->get();
        return view('admin.pages.users.create', ['roles' => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->checkRole($this->roles);
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        $inputs = $request->all();
        $inputs['password'] = bcrypt($inputs['password']);
        User::create($inputs);

        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->checkRole($this->roles);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->checkRole($this->roles);
        $roles = Roles::where('name', '!=', 'Super')->get();
        $user = User::where('id', $id)->firstOrFail();
        return view('admin.pages.users.edit', ['roles' => $roles, 'user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->checkRole($this->roles);
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6|confirmed',
        ]);
        $inputs = $request->all();
        $inputs['password'] = bcrypt($inputs['password']);
        User::find($id)->update($inputs);

        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->checkRole($this->roles);
        User::destroy($id);
        return redirect()->route('users.index');
    }
}
