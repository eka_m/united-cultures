<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PagesController extends GlobalController
{

    private $roles = ['Super','Admin','Moderator'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->checkRole($this->roles);
        try {
            $pages = Page::orderBy('pos', 'ASC')->get();
            $pages = self::mapTree($pages);
            return view('admin.pages.pages.pages', ['pages' => $pages]);
        } catch (\Exception $e) {
        }
    }

    private static function mapTree($dataset, $parent = 0)
    {
        $tree = array();
        foreach ($dataset as $id => $node) {
            if ($node->parent_id != $parent) continue;
            $node->children = self::mapTree($dataset, $node->id);
            $tree[$id] = $node;
        }
        return $tree;
    }


    public function sort(Request $request)
    {
        $data = $request->all();
        foreach ($data as $value) {
            $page = Page::find($value['id']);
            $page->pos = $value['order'];
            $page->parent_id = isset($value['parentId']) ? $value['parentId'] : 0;
            $page->save();
        }
        return response()->json(['message' => 'Сортировка выполнена'], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->checkRole($this->roles);
        try {
            $pages = Page::all();
            $page = new Page();
            return view('admin.pages.pages.create', compact('page','pages'));
        } catch (\Exception $e) {

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->checkRole($this->roles);
        // try {
            $inputs = $request->all();
            Page::create($inputs);
            $notification = [
                'type' => 'success',
                'message' => 'Page Saved'
            ];
            return redirect()->route('pages.index')->with('notification', $notification);
        // } catch (\Exception $e) {

        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page $page
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->checkRole($this->roles);
        $page = Page::where('id', $id)->with('parent')->firstOrFail();
        $pages = Page::all();
        return view('admin.pages.pages.edit', compact('page','pages'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Page $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        $this->checkRole($this->roles);
        try {
            $inputs = $request->all();
            $page->update($inputs);
            $notification = [
                'type' => 'success',
                'message' => 'Page Updated'
            ];
            return redirect()->route('pages.index')->with('notification', $notification);
        } catch (\Exception $e) {
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page $page
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->checkRole($this->roles);
        try {
            $page = Page::where('id', '=', $id)->with('children')->first();
            $children = $page->children;
            $page->delete();
            if (count($children) > 0) {
                foreach ($children as $child) {
                    $this->destroy($child['id']);
                }
            }
            return response()->json(['message' => 'Deleted'], 200);
        } catch (\Exception $e) {
            return response()->json([], 500);
        }
    }
}
