<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Banner;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\GlobalController;


class BannersController extends GlobalController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $banners = Banner::orderBy('created_at', 'DESC')->paginate(10);
            return view('admin.pages.banners.banners', ['banners' => $banners]);
        } catch (\Exception $e) {

        }
    }

    public function async()
    {
        try {
            $banners = Banner::orderBy('created_at', 'DESC')->paginate(10);
            return response()->json(['banners' => $banners, 'message' => 'Banners loaded'], 200);
        } catch (\Exception $e) {

        }
    }

    public function search(Request $request)
    {
        try {
            $field = $request->get('searchfield');

            $banners = Banner::where('name', 'like', '%' . $field . '%')->orderBy('created_at', 'DESC')->paginate(10);
            if (empty($field) || !$banners->count()) {
                $notification = [
                    'message' => 'Nothing found',
                    'type' => 'danger'
                ];
                return redirect()->route('banners.index')->with('notification', $notification);
            }
            return view('admin.pages.banners.banners', ['banners' => $banners]);
        } catch (\Exception $e) {

        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            if(!$request->input('image')) {
                return response()->json(['message' => 'Image required', 'type' => 'warning'], 200);
            }
            Banner::create($request->all());
            return response()->json(['message' => 'Banner added'], 200);
        } catch (\Exception $e) {
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banner $banner
     * @return \Illuminate\Http\Response
     */
    public function edit(Banner $banner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Banner $banner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banner $banner)
    {
        try {
            if(!$request->input('image')) {
                return response()->json(['message' => 'Image required', 'type' => 'warning'], 200);
            }
            $banner->update($request->all());
            return response()->json(['message' => 'Banner updated'], 200);
        } catch (\Exception $e) {
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banner $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $banner)
    {
        try {
            $banner->delete();
            return response()->json(['message' => 'Banner deleted'],200);
        } catch (\Exception $e) {

        }
    }
}
