<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Admin\Report;
use Illuminate\Support\Facades\DB;
class ReportsController extends GlobalController
{
    private $roles = ['Super','Admin','Moderator'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $reports = Report::orderBy('pos', 'DESC')->paginate(10);
            return view('admin.pages.reports.reports', compact('reports'));
        } catch (\Exception $e) {

        }
    }

    public function search(Request $request)
    {
        try {
            $field = $request->get('searchfield');
            $reports = Report::where('name', 'like', '%' . $field . '%')->orderBy('pos', 'DESC')->paginate(10);
            if (empty($field) || count($reports) == 0) {
                $notification = [
                    'message' => 'Nothing found',
                    'type' => 'danger'
                ];
                return redirect()->route('reports.index')->with('notification', $notification);
            }
            return view('admin.pages.reports.reports', compact('reports'));
        } catch (\Exception $e) {

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $report = new Report();
            return view('admin.pages.reports.create', compact('report'));
        } catch (\Exception $e) {

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $oldPos = DB::table('reports')->max('pos');
            $inputs = $request->all();
            $inputs['pos'] = $oldPos + 1;
            Report::create($inputs);
            $notification = [
                'type' => 'success',
                'message' => 'Report Saved'
            ];
            return redirect()->route('reports.index')->with('notification', $notification);
        } catch (\Exception $e) {

        }
    }

    public function setStatus($id)
    {
        $this->checkRole($this->roles);
        try {
            $report = Report::find($id);
            $report->status = !$report->status;
            $report->save();
            return response()->json(['message' => 'Status updated']);
        } catch (\Exception $e) {
            return response()->json([], 500);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Report $report
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Report $report
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $report = Report::find($id);
            return view('admin.pages.reports.edit', compact('report'));
        } catch (\Exception $e) {

        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Report $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $report)
    {
        try {
            $inputs = $request->all();
            $report->update($inputs);
            $notification = [
                'type' => 'success',
                'message' => 'Report Updated'
            ];
            return redirect()->route('reports.index')->with('notification', $notification);
        } catch (\Exception $e) {
        }
    }

    public function sort(Request $request, $id)
    {
        $this->checkRole($this->roles);
        try {
            $pos = $request->input('newpos');
            $currentpos = $request->input('pos');
            if ($pos === 1) {
                $oldPos = Report::where('pos', '<', $currentpos)->max('pos');
            } elseif ($pos === -1) {
                $oldPos = Report::where('pos', '>', $currentpos)->min('pos');
            }
            if ($oldPos) {
                $oldArticle = Report::where('pos', $oldPos)->first();
                $prevpos = $oldArticle->pos;
                $oldArticle->update(['pos' => $currentpos]);
                $report = Report::find($id);
                $report->update(['pos' => $prevpos]);
                return response()->json(['message' => 'Sorted'], 200);
            } else {
                return response()->json(['message' => 'Not sorted', 'type' => 'warning'], 200);
            }
        } catch (\Exception $e) {
            return response()->json([], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Report $report
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        $this->checkRole($this->roles);
        try {
            $report->delete();
            return response()->json(['message' => 'Deleted'], 200);
        } catch (\Exception $e) {
            return response()->json([], 500);
        }

    }
}
