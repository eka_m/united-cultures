<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
class ProductsController extends GlobalController
{
    public function all($slug) {

        try {
            $reports = Product::orderBy('pos','DESC')->get();
            return view('pages.reports.index', compact('reports'));
        } catch(\Exception $e) {

        }
    }
}
