<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Models\Locale;
use App\Models\Category;

class PagesController extends GlobalController
{
    public function show($url)
    {
        $page = Page::where('url', $url)->firstOrFail();
        return call_user_func_array([$this, $page->type.'Page'], [$page]);
    }

    public function routePage($page) {
        return redirect('/'.$this->locale.'/'.$page->url);
    }

    public function regularPage($page) {
        $articles = $page->articles()
        ->orderBy('pos', 'DESC')->paginate(6);
        $parsedContent = $this->getGallery($page->content[$this->locale]);
        $page->content = $parsedContent['content'];
        $gallery =  $parsedContent['gallery'];
        return view('pages.pages.page', compact('page', 'gallery', 'articles'));
    }

}