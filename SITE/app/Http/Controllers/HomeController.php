<?php

namespace App\Http\Controllers;

use App\Models\Video;
use App\Models\Report;
use App\Models\Article;
use App\Models\Gallery;

use App\Models\Setting;
use Illuminate\Http\Request;

class HomeController extends GlobalController
{
    public function show()
    {
        try {
            $slider = Setting::take('main-slider');
            $revents = Article::where('interesting',1)->where('status',1)->orderBy('pos','DESC')->limit(3)->get();
            $articles = Article::where('status', 1)->where('status',1)->orderBy('pos','DESC')->limit(3)->get();
            $nextevent = Article::where('next', 1)->where('status',1)->first();
            $galleryOfTheMonth = Gallery::where('month',1)->first();
            $videos = Video::orderBy('created_at','DESC')->get();
            $shortabout = Setting::take('short-about');
            $contactform = Setting::take('contact-form');
            $report = Report::orderBy('pos','DESC')->where('slug','report')->where('status',1)->first();
            $profile = Report::orderBy('pos','DESC')->where('slug','profile')->where('status',1)->first();
            return view('pages.home.index', compact('slider', 'shortabout', 'contactform', 'galleryOfTheMonth', 'revents', 'articles', 'nextevent','report','profile','videos'));
        } catch (\Exception $e) {

        }
    }

    public function soon() {
        return view('partials.soon');  
    }
    
    public function search(Request $request)
    {
        try {
            $field = $request->get('query');
            $articles = Article::where('name', 'like', '%' . $field . '%')
            ->orWhere('content', 'like', '%' . $field . '%')
            ->where('status', 1)
            ->orderBy('created_at', 'DESC')
            ->with('page')
            ->paginate(6);
            return view('partials.search-result', ['articles' => $articles]);
        } catch (\Exception $e) {

        }
    }
}
