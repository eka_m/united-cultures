<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactForm;
use App\Models\Setting;
use Illuminate\Support\Facades\Mail;
class FormsController extends Controller
{
    protected function contactForm(Request $request)
    {
        try {
            $data = $request->all();
            $form = Setting::take('contact-form');
            $receivers = explode(',', $form->content['receivers']);
            Mail::to($receivers)->send(new ContactForm($data));
            return response()->json(['type' => 'success', 'title' => 'Dear '.$request->input('name'), 'message' => $form->content['success']],200);
        } catch(\Exception $e) {
            return response()->json(['type' => 'error', 'title' => 'Error', 'message' => 'Oooooops... Sorry something went wrong. Please try again later.'],500);
        }
    }
}
