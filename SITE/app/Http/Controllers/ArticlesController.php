<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class ArticlesController extends GlobalController
{

    public function show($url)
    {
        $article = Article::where('url', $url)->firstOrFail();
        $next = $article->next();
        $prev = $article->previous();
        $revents = Article::where('interesting',1)->where('status',1)->limit(3)->get();
        $article->views = $article->views + 1;
        $article->fakeviews = $article->fakeviews + 1;
        $article->save();
        $parsedContent = $this->getGallery($article->content[$this->locale]);
        $article->content = $parsedContent['content'];
        return view('pages.articles.article', ['article' => $article, 'gallery'=> $parsedContent['gallery'], 'revents' => $revents, 'next' => $next, 'prev' => $prev]);
    }

    public function byCategory($id) {
        if($id == 0) {
            $articles = Article::orderBy('pos', 'DESC')->paginate(3);
        } else {
            $articles = Article::where('page_id', $id)->orderBy('pos', 'DESC')->paginate(3);
        }
        return response()->json($articles);
    }
}
