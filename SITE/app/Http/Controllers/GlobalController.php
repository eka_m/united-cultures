<?php

namespace App\Http\Controllers;


use App\Models\Brief;
use App\Models\Locale;
use App\Models\Page;
use App\Models\Gallery;
use App\Models\Setting;

use Illuminate\Http\Request;
use View;
use DOMDocument;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;


class GlobalController extends Controller
{
    protected $locale = 'az';

    public function __construct(Request $request)
    {
        $this->locale = LaravelLocalization::getCurrentLocale();
        $clearpages = Page::orderBy('pos', 'ASC')->where('place', 'mainmenu')->orWhere('place', 'both')->with('childrens')->get();
        $categories = Page::orderBy('pos', 'ASC')->where('place', 'category')->orWhere('place', 'both')->get();
        $pressbriefly = Brief::orderBy('pos', 'ASC')->where('status',1)->get();
        $allLocales = Locale::orderBy('pos', 'ASC')->where('slug', '!=', $this->locale)->get();
        $pages = self::mapTree($clearpages);
        $pages = self::prepareMenu($pages, 0, $request->url());
        $seo = Setting::take('seo');
        $social = Setting::take('social');
        View::share('seo', $seo);
        View::share('social', $social);
        View::share('pages', $pages);
        View::share('press', $pressbriefly);
        View::share('categories', $categories);
        View::share('clearpages', $clearpages);
        View::share('currentlocale', $this->locale);
        View::share('locales', $allLocales);
    }

    private static function mapTree($dataset, $parent = 0)
    {
        $tree = array();
        foreach ($dataset as $id => $node) {
            if ($node->parent_id != $parent) continue;
            $node->children = self::mapTree($dataset, $node->id);
            $tree[$id] = $node;
        }
        return $tree;
    }

    private static function prepareMenu($array, $i = 0, $path)
    {

        $tree = '<ul class="menu">';
        if ($i > 0) {
            $tree = '<ul class="sub-menu">';
        }

        foreach ($array as $item) {
            $class = ends_with($path, $item->url) ? 'class="active"' : '';

            $url = ($item->type == 'route' ? '' : '/page/') . $item->url;

            switch($item->url){
                case '#';
                $url = "javascript:void(0)";
                break;
            }

            $tree .= '<li '.$class.'><a href="'. $url .'" ' . $class . '>' . $item->name[LaravelLocalization::getCurrentLocale()] . '</a>';


            
            if ($item->children && !empty($item->children)) {
                $tree .= self::prepareMenu($item->children, $i + 1, $path);
            }
            $tree .= '</li>';
            $i++;
        }
        $tree .= '</ul>';
        return $tree;
    }


    protected function getGallery($html)
    {
        if(!$html && $html == '') return $html;
        $libxmlPreviousState = libxml_use_internal_errors(true);
        $dom = new DOMDocument;
        $dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
        libxml_clear_errors();
        libxml_use_internal_errors($libxmlPreviousState);
        $elements = $dom->getElementsByTagName('gallery');
        $gallery = [];
        for ($i = $elements->length - 1; $i >= 0; $i--) {
            $element = $elements->item($i);
            $id_element = $element->getAttribute("id");
            $dbgallery =  Gallery::find($id_element);
            if(!$dbgallery) return $html;
            $gallery[] =$dbgallery;
            $params = json_decode($dbgallery->params, true);
            $view = View::make('partials.unite-gallery', ["gallery" => $dbgallery, "params" => $params]);
            $template = $view->render();
            $template = (string)$view;
            if ($template) {
                $newdoc = new DOMDocument;
                $newdoc->loadHTML(mb_convert_encoding($template, 'HTML-ENTITIES', 'UTF-8'));
                $element->parentNode->replaceChild($dom->importNode($newdoc->documentElement, TRUE), $element);
            }
        }
        return ["content" => $dom->saveHTML(), 'gallery' => empty($gallery) ? [] : $gallery];
    }

}