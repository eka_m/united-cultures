import Vue from 'vue';
import MdImage from './Image.vue'
export const MediumImage = Vue.extend({
    template: '<md-image :url="url" :from="from"></md-image>',
    components: {MdImage}
});
