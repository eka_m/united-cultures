import {MediumImage} from '../Plugins/Image'
export const helpers = {
    methods: {
        createImage (url = false, place = false, elem = false) {
            url = url ? url : $(elem).children('img').attr('src');
            place = place ? place : elem;
            const image = new MediumImage({
                    data: {
                        url: url,
                        from: elem
                    }}).$mount();
            $(this.$refs.editor).find(place).replaceWith(image.$el);
        },
        initRemovableItem () {
            $(this.$refs.editor).children().each(function () {
                $(this).contextmenu(() => {
                    return false
                });

                $(this).contextmenu(function () {
                    $(this).addClass('md__editor__selected');
                    if ($(this).children('.delete_editor_elem').length === 0) {
                        $(this).append('<div class="delete_editor_elem" title="Remove" contenteditable="false"><i class="dj-trash"  contenteditable="false"></i></div>');
                    }
                });

                $(this).on('mouseleave', function () {
                    $(this).removeClass('md__editor__selected').children('.delete_editor_elem').remove();
                });
            });
        }
    }
};
