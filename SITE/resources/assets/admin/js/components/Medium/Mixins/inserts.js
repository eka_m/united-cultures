export const inserts = {
    methods: {
        addRow () {
            let elem = $('<p data-placeholder="Type your text" class="uk-animation-slide-left"></p>').clone();
            $(this.$refs.editor).append(elem);
            this.setResult();
            this.caretToEnd();
        },
        addHr () {
            this.editor.pasteHTML('<hr size="3" style="width: 90%; margin: 0 auto; ">', {cleanAttrs: []});
            this.setResult();

        },
        addVideo () {
            const videoPlaceholder = '<p id="videoPlaceholder" data-placeholder="Video Placeholder"></p>';
            this.editor.pasteHTML(videoPlaceholder, {cleanAttrs: []});
            this.setResult();

        },
        addImage () {
            const inst = this;
            moxman.browse({
                view: 'thumbs',
                oninsert (args) {
                    inst.editor.pasteHTML("<p class='imageplace'></p>", {cleanAttrs: []});
                    inst.createImage('/uploads' + args.focusedFile.path, '.imageplace');
                    inst.setResult();
                }
            });
        },
    }
};