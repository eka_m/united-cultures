import caret from 'jquery.caret'
export const caretHelper = {
    data () {
        return {
            caretPos: '3'
        }
    },
    watch: {
        caretPos () {
            this.$refs.floatingtoolbar.toolbarPos(this.caretPos);
        }
    },
    methods: {
        caretPosition() {
            const caretpos = $(this.$refs.editor).caret('position');
            if (caretpos.top < 3) {
                caretpos.top = 3
            }
            this.caretPos = caretpos.top;
        },
        caretToEnd() {
            const el = this.$refs.editor;
            el.focus();
            if (typeof window.getSelection != "undefined"
                && typeof document.createRange != "undefined") {
                var range = document.createRange();
                range.selectNodeContents(el);
                range.collapse(false);
                var sel = window.getSelection();
                sel.removeAllRanges();
                sel.addRange(range);
            } else if (typeof document.body.createTextRange != "undefined") {
                var textRange = document.body.createTextRange();
                textRange.moveToElementText(el);
                textRange.collapse(false);
                textRange.select();
            }
        },
    }
};