import MediumEditor from 'medium-editor';

// import './html-editor.fontsize.extension.scss';


/**
 * CompanyFontSizes
 * A simple extension to allow the selection of font size directly in the action toolbar
 */
const CompanyFontSizes = MediumEditor.Extension.extend({

    name: 'companySizes',

    /**
     * Stores all the possible pickable font size
     */
    fontSizes: ['', '1', '2', '3', '4', '5', '6', '7'],

    /**
     * @inheritDoc
     */
    constructor(options) {
        MediumEditor.Extension.call(this, options);
    },

    /**
     * @inheritDoc
     */
    init() {
        this.buttonContainer = this.document.createElement('div');
        this.buttonContainer.classList.add('medium-editor-button-container');

        // Font Name Form (div)
        const form = this.document.createElement('form');
        form.classList.add('medium-editor-fontsize-container');
        form.classList.add('medium-editor-form-container');
        form.id = 'medium-editor-toolbar-form-fontsize-' + this.getEditorId();
        this.buttonContainer.appendChild(form);

        const select = this.document.createElement('select');
        select.classList.add('medium-editor-form-select');
        form.appendChild(select);

        // Add font sizes
        this.fontSizes.forEach((item) => {
            const option = this.document.createElement('option');
            option.innerHTML = item;
            option.value = item;
            select.appendChild(option);
        });

        // Attach editor events to keep status updates
        this.attachToEditables();

        // Handle typing in the text box
        this.on(select, 'change', event => this.handleFontSizeChange(event));
    },

    getSelect() {
        return this.getButton().querySelector('select.medium-editor-form-select');
    },


    /**
     * @inheritDoc
     */
    attachToEditables() {
        this.subscribe('positionedToolbar', event => this.handlePositionedToolbar(event));
    },

    /**
     * @inheritDoc
     */
    deattachFromEditables() {
        this.base.unsubscribe('positionedToolbar', event => this.handlePositionedToolbar(event));
    },

    /**
     * @inheritDoc
     */
    handlePositionedToolbar(event) {
        // get the current selection when toolbar appear so we can retrieve the font used
        // by this selection
        const fontSize = this.document.queryCommandValue('fontSize') + '';
        this.updateSelection(fontSize);
    },

    /**
     * Update the selection of the combo box
     * @param value the item to be selected
     */
    updateSelection(value) {
        const select = this.getSelect();
        select.value = value || '';
    },

    /**
     * @inheritDoc
     */
    handleFontSizeChange(event) {
        const size = this.getSelect().value;
        if (size === '') {
            this.clearFontSize();
        } else {
            this.execAction('fontSize', { value: size });
        }
    },

    /**
     * @inheritDoc
     */
    clearFontSize() {
        MediumEditor.selection.getSelectedElements(this.document).forEach((el) => {
            if (el.nodeName.toLowerCase() === 'font' && el.hasAttribute('size')) {
                el.removeAttribute('size');
            }
        });
    },

    /**
     * @inheritDoc
     */
    getButton() {
        return this.buttonContainer;
    },

    /**
     * @inheritDoc
     */
    destroy() {
        this.deattachFromEditables();
    },
});


MediumEditor.extensions.companysizes = CompanyFontSizes;

export default CompanyFontSizes;