/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import './bootstrap.js';
import Vue from 'vue';
import UIkit from 'uikit';
import VueBus from 'vue-bus';
import Icons from 'uikit/dist/js/uikit-icons';

window.UIkit = UIkit;
window.S = require('string');
Vue.use(VueBus);
UIkit.use(Icons);

axios.interceptors.response.use(r => {
    switch (r.status) {
        case 200:
            UIkit.notification({
                message: r.data.message,
                status: r.data.type ? r.data.type : 'success',
                pos: 'top-right',
                timeout: 3000
            });
            break;
        case 500:
            UIkit.notification({
                message: 'Server Error',
                status: 'danger',
                pos: 'top-right',
                timeout: 3000
            });
            break;
    }
    return r;
});
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('tree', r =>
    require.ensure([], () => r(require('./components/Tree/Tree.vue')), 'tree')
);

Vue.component('fileInput', r =>
    require.ensure([], () => r(require('./components/FileInput/fileinput.vue')), 'file-input')
);

Vue.component('imageinput', r =>
    require.ensure(
        [],
        () => r(require('./components/ImageInput/ImageInput.vue')),
        'imageinput'
    )
);
Vue.component('range', r =>
    require.ensure([], () => r(require('./components/Range/Range.vue')), 'range')
);
Vue.component('article-item', r =>
    require.ensure(
        [],
        () => r(require('./components/Articles/Article.vue')),
        'article'
    )
);
Vue.component('report', r =>
    require.ensure(
        [],
        () => r(require('./components/Reports/Report.vue')),
        'reports'
    )
);
Vue.component('brief', r =>
    require.ensure(
        [],
        () => r(require('./components/Briefs/Brief.vue')),
        'briefs'
    )
);
Vue.component('translate', r =>
    require.ensure(
        [],
        () => r(require('./components/Articles/Translate.vue')),
        'translate'
    )
);
Vue.component('locales', r =>
    require.ensure(
        [],
        () => r(require('./components/Locale/Locales.vue')),
        'locale'
    )
);
Vue.component('videos', r =>
    require.ensure(
        [],
        () => r(require('./components/Videos/Videos.vue')),
        'videos'
    )
);
Vue.component('slider', r =>
    require.ensure(
        [],
        () => r(require('./components/Slider/Slider.vue')),
        'slider'
    )
);
Vue.component('banner', r =>
    require.ensure(
        [],
        () => r(require('./components/Banners/Banners.vue')),
        'banner'
    )
);
Vue.component('pagination', r =>
    require.ensure([], () => r(require('laravel-vue-pagination')), 'pagination')
);
Vue.component('flatpickr', r =>
    require.ensure(
        [],
        () => r(require('./components/Partials/flatpickr.vue')),
        'flatpickr'
    )
);
Vue.component('datepicker', r =>
    require.ensure(
        [],
        () => r(require('./components/Partials/Datepicker.vue')),
        'datepicker'
    )
);
Vue.component('editor', r =>
    require.ensure(
        [],
        () => r(require('./components/Partials/Editor.vue')),
        'editor'
    )
);
Vue.component('popup', r =>
    require.ensure(
        [],
        () => r(require('./components/Settings/Popup.vue')),
        'popup'
    )
);
Vue.component('mediamanager', r =>
    require.ensure(
        [],
        () => r(require('./components/Mediamanager/Mediamanager.vue')),
        'mediamanager'
    )
);
Vue.component('slug', r =>
    require.ensure(
        [],
        () => r(require('./components/Translit/translit.vue')),
        'sluggenerator'
    )
);
Vue.component('events', r =>
    require.ensure(
        [],
        () => r(require('./components/Events/events.vue')),
        'weeklyevents'
    )
);
Vue.component('attachArticle', r =>
    require.ensure(
        [],
        () => r(require('./components/Events/attacharticle.vue')),
        'attacharticle'
    )
);
Vue.component('galleries', r =>
    require.ensure(
        [],
        () => r(require('./components/Galleries/Galleries.vue')),
        'galleries'
    )
);
Vue.component('galleryImages', r =>
    require.ensure(
        [],
        () => r(require('./components/Galleries/images.vue')),
        'galleries'
    )
);

Vue.component('localeToggler', r => require.ensure([], () => r(require('./components/LocaleToggler/localetoggler.vue')), 'localetoggler'));
Vue.component('localizedInput', r => require.ensure([], () => r(require('./components/LocalizedInput/localizedinput.vue')), 'localizedinput'));
Vue.component('localizedTextarea', r => require.ensure([], () => r(require('./components/LocalizedTextarea/localizedTextarea.vue')), 'localizedinput'));
const app = new Vue({
    el: '#app'
});
