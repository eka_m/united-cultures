$( document ).ready( () => {
	/* AOS */
	AOS.init( {
		offset: 200,
		duration: 600,
		easing: 'ease-in-back',
		delay: 200,
		once: true
	} );
	/* END AOS */
	$( ".loader" ).fadeOut();
	$( "#loading-overlay" ).delay( 300 ).fadeOut( 'slow', function () {
		$( this ).remove();
		$( "html" ).toggleClass( "overflow" );
	} );
	/* PLYR */
	if ( $( ".uc-tv-player-box" ).length ) {
		let player = Plyr.setup( '.uc-tv-player-box' )[ 0 ];
		$( '.uc-tv__player__thumbs__nav--up' ).click( function ( e ) {
			$( '.uc-tv__player__thumbs__container' ).animate( {
				scrollTop: $( '.uc-tv__player__thumbs__container' ).scrollTop() - $( '.uc-tv__player__thumbs__container__thumb' ).height()
			}, 'slow' );
		} );
		$( '.uc-tv__player__thumbs__nav--down' ).click( function ( e ) {
			e.preventDefault();
			$( '.uc-tv__player__thumbs__container' ).animate( {
				scrollTop: $( '.uc-tv__player__thumbs__container' ).scrollTop() + $( '.uc-tv__player__thumbs__container__thumb' ).height()
			}, 100 );
		} );
		$( document ).on( 'click', '.uc-tv__player__thumbs__container__thumb--title', function () {
			// player.destroy();
			var el = $( this ).parent();
			var type = el.data( 'type' );
			var link = el.data( 'link' );
			player.source = {
				type: 'video',
				sources: [ {
					src: link,
					provider: type
				} ]
			};
			player.on( 'ready', event => {
				player.play();
			} );
		} );
		player.on( 'ready', event => {
			$( '.uc-tv__player__thumbs' ).height( $( '.plyr' ).height() );
		} );
		$( window ).resize( function () {
			$( '.uc-tv__player__thumbs' ).height( $( '.plyr' ).height() );
		} );
	}
	/* END PLYR */
	$( '.uc-tv__player__thumbs__container__thumb' ).hover( function () {
		$( this ).find( '.uc-tv__player__thumbs__container__thumb--title' ).toggleClass( 'slideInRight' );
	} )
	/* MENU */
	var overlayElem = '<div class="overlay"></div>';
	$( ".toggle-wrapper-menu" ).click( function () {
		closePressBriefly();
		toggleMainMenu();
	} );
	$( ".toggle-press-briefly" ).click( function () {
		closeMainMenu();
		togglePressBriefly();
	} );
	$( document ).on( "click", ".overlay", function () {
		closeMainMenu();
		closePressBriefly();
	} );

	function toggleMainMenu () {
		$( ".toggle-wrapper-menu" ).toggleClass( "open" );
		$( ".main-nav" ).toggleClass( "opened" );
		$( "html" ).toggleClass( "overflow" );
		overlay();
	}

	function togglePressBriefly () {
		$( ".toggle-press-briefly" ).toggleClass( "open" );
		$( ".press-briefly" ).toggleClass( "opened" );
		$( "html" ).toggleClass( "overflow" );
		overlay();
	}

	function closeMainMenu () {
		if ( $( ".toggle-wrapper-menu" ).hasClass( "open" ) ) {
			$( ".toggle-wrapper-menu" ).removeClass( "open" );
			$( ".main-nav" ).removeClass( "opened" );
			$( "html" ).removeClass( "overflow" );
			overlay( "remove" );
		}
	}

	function closePressBriefly () {
		if ( $( ".toggle-press-briefly" ).hasClass( "open" ) ) {
			$( ".toggle-press-briefly" ).removeClass( "open" );
			$( ".press-briefly" ).removeClass( "opened" );
			$( "html" ).removeClass( "overflow" );
			overlay( "remove" );
		}
	}

	function overlay ( type = "toggle" ) {
		if ( type === "add" ) {
			$( "body" ).append( overlayElem );
		} else if ( type === "remove" ) {
			$( "body" ).find( ".overlay" ).remove();
		} else {
			if ( $( "body" ).has( ".overlay" ).length ) {
				$( "body" ).find( ".overlay" ).remove();
			} else {
				$( "body" ).append( overlayElem );
			}
		}
	}

	/* END MENU */
	/* UNITE GALLERY */
	if ( $( "#monthgallery" ).length ) {
		$( "#monthgallery" ).unitegallery( {
			slider_control_zoom: false,
			slider_enable_zoom_panel: false,
			theme_enable_fullscreen_button: true,
			slider_enable_text_panel: true,
			gallery_autoplay: true, //true / false - begin slideshow autoplay on start
			gallery_play_interval: 5000, //play interval of the slideshow
			gallery_pause_on_mouseover: true,
			gallery_control_thumbs_mousewheel: true,
			gallery_height: 600,
			thumb_height: 80,
			thumb_width: 120,
		} );
	}
	/* END UNITE GALLERY */

	/* MOUSEWHELL */
	function mousewhell () {
		$( "#article-read" ).bind( "mousewheel", function ( e ) {
			var padding = $( ".article-read__cover__title" ).height();
			if ( e.deltaY == 7 ) {
				$( ".article-read__rightpanel" ).removeClass( "show" );
				$( ".article-read__cover__title" ).removeClass( "topanel" );
				// $('body').toggleClass('overflow');
			} else if ( e.deltaY < 0 ) {
				$( ".article-read__rightpanel" ).addClass( "show" ).find( ".article-read__rightpanel__content" ).css( {
					transform: "translateY(" + padding + "px)"
				} );
				$( ".article-read__cover__title" ).addClass( "topanel" );
				// $('body').toggleClass('overflow');
			}
		} );
	}

	if ( $( "window" ).with > 768 ) {
		mousewhell();
	}
	$( window ).on( "resize", function () {
		var win = $( this );
		if ( win.width() > 768 ) {
			mousewhell();
		}
	} );
	/* END MOUSEWHELL */

	/*SHORT ABOUT SECTION*/
	const image = $( "#categoryImage" );
	const link = $( ".categoryLink" );
	const defaultImage = image.attr( 'src' );
	link.hover( function ( e ) {
		image.attr( "src", $( this ).data( "image" ) );
	}, function (  ) {
		image.attr( "src", defaultImage );
	} );
	/*END SHORT ABOUT SECTION*/
} );