
import './bootstrap.js';
import Vue from 'vue';

Vue.component('articles', r => require.ensure([], () => r(require('./components/articles/articles.vue')), 'articles'));
const app = new Vue({
    el: '#app'
});
