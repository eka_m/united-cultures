<!doctype html>
<html lang="{{$currentlocale}}" class="overflow">

<head>
    @include('partials.meta_tags')
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families": ["Great Vibes","Pinyon Script", "Overlock:400,400i,700,700i,900,900i","Simonetta:400,400i,900,900i"]},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    @include('partials.stylesheets')
</head>

<body class="h-100">
    <div id="loading-overlay" class="loading-overlay">
        <div class="loader">
            <img src="{{asset('images/loader.gif')}}">
        </div>
    </div>
    <div class="main-wrapper h-100" id="app">
        @include('partials.mobile_toppanel')
        @include('partials.press_briefly')
        @include('partials.nav')
        <main>
            <div class="container main-container">
                @yield('content')
            </div>
        </main>
    </div>
    @include('partials.footer')
    @include('partials.scripts')
</body>

</html>