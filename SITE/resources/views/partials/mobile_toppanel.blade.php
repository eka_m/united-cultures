<div class="mobile-toppanel d-sm-none pt-2">
        <div class="row">
            <div class="col-6 pl-4">
                <div class="press-briefly-toggler">
                    <div class="toggle-press-briefly">
                        <a href="javascript:void(0)" class="link-reset">PRESS BRIEFLY</a>
                    </div>
                </div>
            </div>
            <div class="col-6 d-flex justify-content-end pr-4">
                <div class="wrapper-menu toggle-wrapper-menu">
                    <div class="line-menu half start"></div>
                    <div class="line-menu"></div>
                    <div class="line-menu half end"></div>
                </div>
            </div>
        </div>
    </div>