<div id="logo" class="logo">
        <a href="{{route('home')}}"><img src="/images/logo.png" alt="United Cultures logo" class="img-fluid"></a>
        {{-- <h1 class="d-block logo__text px-lg-5">Cultural Diplomacy in Azerbaijan</h1> --}}
        <div class="social">
                <a href="{{$social->content['facebook']}}"><img src="/images/facebook-icon.svg" alt="facebook-icon" class="facebook-icon"></a>
                <a href="{{$social->content['instagram']}}"><img src="/images/instagram-icon.svg" alt="instagram-icon" class="instagram-icon"></a>
        </div>
</div>