<div class="press-briefly offcanvas">
    <div class="press-briefly-content">
        <ul>
            @foreach($press as $item)
            <li>
                <a href="{{asset('/uploads'.$item->file)}}" target="_blank">
                        <img src="{{asset('uploads'.$item->image)}}" alt="">
                        <h3>{{$item->name[$currentlocale]}}.</h3>
                    </a>
            </li>
            @endforeach
        </ul>
    </div>
    <div class="press-briefly__toggler p-0 d-flex align-items-center justify-content-center border">
        <div class="toggle-press-briefly">
            <a href="javascript:void(0)" class="link-reset">PRESS BRIEFLY</a>
        </div>
    </div>
</div>