<div id="main-slider" class="main-slider">
        @foreach($slider['content'] as $slide)
        <div class="main-slider__slide">
            <img src="{{asset('uploads'.$slide['path'])}}" alt="" class="img-fluid">
            @if(isset($slide['title'][$currentlocale]))
                @php
                $date = explode(',',$slide['title'][$currentlocale])
                @endphp
                <div class="main-slider__slide--date" 
                data-animation-in="rollInRight" 
                data-delay-in="0.4">
                    <span>
                        @foreach($date as $item)
                            <span>{{$item}}</span>
                        @endforeach
                    </span>
                </div>
            @endif
            @if(isset($slide['description'][$currentlocale]))
                <div class="main-slider__slide--title" 
                data-animation-in="fadeInUp" 
                data-delay-in="0.6">
                    <a href="{{$slide['link'] or 'javascript:void(0)'}}">{!! $slide['description'][$currentlocale] !!}</a>
                </div>
            @endif
        </div>
        @endforeach
    </div>