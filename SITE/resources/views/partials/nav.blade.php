<div class="row main-nav offcanvas" id="main-nav">
    <div class="col-2 p-0 d-flex align-items-center justify-content-center h-100 border">
        <div class="d-none d-sm-block main-nav__toggler">
            {{-- <div class="wrapper-menu toggle-wrapper-menu">
                <div class="line-menu half start"></div>
                <div class="line-menu"></div>
                <div class="line-menu half end"></div>
            </div> --}}
            <div class="toggle-main-nav">
                <a href="javascript:void(0)" class="link-reset toggle-wrapper-menu">MENU</a>
            </div>
        </div>
    </div>
    <div class="col-8 d-flex align-items-center justify-content-center h-100">
            {!! $pages !!}
    </div>
</div>