<div class="uk-margin">
    <div class="uk-form-label uk-animation-slide-bottom">Name</div>
    <localized-input input-name="name" 
    class-name="uk-input uk-form-large" 
    id="translit-it" placeholder="Category name" 
    available-data="{{$category->name ? json_encode($category->name) : null}}">
    </localized-input>
</div>
<div class="uk-margin">
    <div class="uk-form-label uk-animation-slide-bottom">Slug</div>
    <slug inputname="slug" oldval="{{old('slug', $category->slug)}}"></slug>
</div>
<div class="uk-margin">
    <div class="uk-form-label uk-animation-slide-bottom">Parent</div>
    <select class="uk-select uk-form-large" name="parent_id">
        <option value="0">No parent</option>
        @foreach($categories as $item)
            @continue($category->id === $item->id)
            <option value="{{$item->id}}"
                    @if($category->parent && $category->parent->id === $item->id) selected @endif>
                    @foreach($locales as $locale)
                    {{$item->name[$locale->slug].'/'}}
                    @endforeach
                </option>
        @endforeach
    </select>
</div>