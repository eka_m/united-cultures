@extends('admin.layouts.base') 
@section('title', '::New Category')
@section('js') 
@parent
<script type="text/javascript" src="{{asset('plugins/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/tinymce/init.js')}}"></script>
@endsection
 
@section('content')
<div class="uk-section">
    <div class="uk-container head-font">
        @include('admin.partials.locale-toggler')
        <form action="{{route('categories.store')}}" method="post">
            {{ csrf_field() }}
            @include('admin.pages.categories._form')
            <div class="uk-margin">
                <button class="uk-button uk-button-secondary uk-float-right">Save</button>
            </div>
        </form>
    </div>
</div>
@endsection