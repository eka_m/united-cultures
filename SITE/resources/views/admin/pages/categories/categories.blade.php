@extends('admin.layouts.base')
@section('title','::Categories')
@section('content')
    <div class="uk-section">
        <div class="uk-container uk-container-small uk-margin-bottom">
            <a href="{{route('categories.create')}}" class="uk-icon-button uk-button-secondary uk-float-right" uk-icon="icon: plus"></a>
        </div>
        <div class="uk-container uk-container-small">
            <tree :pages="{{json_encode($categories)}}" url="categories"></tree>
        </div>
    </div>
@endsection