@extends('admin.layouts.base')
@section('title', '::Banners')
@section('js')
    @parent
    <script type="text/javascript" src="{{asset('plugins/moxiemanager/js/moxman.loader.min.js')}}"></script>
@endsection
@section('content')
    <banner data="{{json_encode($banners)}}"></banner>
@endsection
