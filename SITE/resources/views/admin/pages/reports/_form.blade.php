<div class="uk-margin uk-child-width-1-3" uk-grid>
    <div>
        <div class="uk-form-label uk-animation-slide-bottom">Name</div>
        <localized-input input-name="name" class-name="uk-input uk-form-large" id="translit-it" placeholder="Report name" available-data="{{$report->name ? json_encode($report->name) : null}}">
        </localized-input>
    </div>
    <div>
        <div class="uk-form-label uk-animation-slide-bottom">Slug</div>
        <slug inputname="slug" oldval="{{old('url', $report->slug)}}"></slug>
    </div>
    <div>
        <div class="uk-form-label uk-animation-slide-bottom">Year</div>
        <input type="text" name="year" class="uk-input uk-form-large" value="{{old('url', $report->year)}}" placeholder="Year">
    </div>
</div>
<div class="uk-margin" uk-grid>
    <div class="uk-width-1-3">
        <file-input inputname="file" file="{{old('file', $report->file)}}"></file-input>
    </div>
    <div class="uk-width-2-3">
        <imageinput inputname="image" btn="image" img="{{old('image', $report->image)}}"></imageinput>
    </div>

</div>