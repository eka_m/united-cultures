@extends('admin.layouts.base')
@section('title', '::New report')
@section('js')
    @parent
    <script type="text/javascript" src="{{asset('plugins/moxiemanager/js/moxman.loader.min.js')}}"></script>
@endsection
@section('content')
    <div class="uk-section">
        <div class="uk-container uk-container-small head-font">
            @include('admin.partials.locale-toggler')
            <form action="{{route('reports.store')}}" method="post">
                {{ csrf_field() }}
                @include('admin.pages.reports._form')
                <div class="uk-margin">
                    <button class="uk-button uk-button-secondary uk-float-right">Save</button>
                </div>
            </form>
        </div>
    </div>

@endsection