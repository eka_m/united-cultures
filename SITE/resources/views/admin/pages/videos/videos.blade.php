@extends('admin.layouts.base')
@section('title', '::Videos')
@section('content')
    <videos data="{{json_encode($videos)}}"></videos>
@endsection
