<ul class="uk-switcher uk-margin">
        <li>
            <div class="uk-margin">
                <div class="uk-form-label uk-animation-slide-bottom">Name</div>
                    <localized-input input-name="name" 
                    class-name="uk-input uk-form-large" 
                    id="translit-it" 
                    placeholder="Article name"
                    available-data="{{$article->name ? json_encode($article->name) : null}}">
                    </localized-input>
            </div>
            <div class="uk-margin">
                    <div class="uk-form-label uk-animation-slide-bottom">Slug</div>
                    <slug inputname="url" oldval="{{old('url', $article->url)}}"></slug>
            </div>
            <div class="uk-margin">
                <div class="uk-form-label uk-animation-slide-bottom">Short description</div>
                    <localized-textarea 
                    input-name="short" 
                    class-name="uk-textarea uk-form-large" 
                    :editor="false" 
                    placeholder= "Short description "
                    available-data="{{$article->short ? json_encode($article->short) : null}}">
                    </localized-textarea>
            </div>
            <div class="uk-margin">
                <div class="uk-form-label uk-animation-slide-bottom">Page</div>
                <select class="uk-select uk-form-large" name="page_id">
                    @foreach($pages as $page)
                        <option value="{{$page->id}}"
                                @if($article->page && $article->page->id === $page->id) selected @endif>{{$page->name[env('DEFAULTLOCALE')]}}</option>
                    @endforeach
                </select>
            </div>
            <div class="uk-margin">
                <imageinput inputname="image" img="{{$article->image}}" btn="Choose image"></imageinput>
            </div>
            <div class="uk-margin">
                <imageinput inputname="cover" img="{{$article->cover}}" btn="Choose cover"></imageinput>
            </div>
        </li>
        <li>
            <localized-textarea 
            input-name="content" 
            class-name="uk-textarea uk-form-large" 
            :editor="true" 
            available-data="{{$article->content ? json_encode($article->content) : null}}">
            </localized-textarea>
        </li>
        <li>
            <div class="uk-margin">
                <div class="uk-form-label uk-animation-slide-bottom">Keywords</div>
                    <localized-input input-name="keywords" 
                    class-name="uk-input uk-form-large" 
                    placeholder="Keywords"
                    available-data="{{$article->keywords ? json_encode($article->keywords) : null}}">
                    </localized-input>
            </div>
            <div class="uk-margin">
                <div class="uk-form-label uk-animation-slide-bottom">Description</div>
                    <localized-textarea 
                    input-name="description" 
                    class-name="uk-textarea uk-form-large" 
                    :editor="false"
                    placeholder="Description "
                    available-data="{{$article->description ? json_encode($article->description) : null}}">
                    </localized-textarea>
            </div>
        </li>
        <li>
            <div class="uk-child-width-1-3@s" uk-grid>
                    <div>
                        <div class="uk-form-label uk-animation-slide-bottom">Date</div>
                        <flatpickr class="uk-input uk-form-large" :conf="{enableTime: false,time_24hr:true}"
                                currentdate="{{$article->date or ''}}" inputname="date"></flatpickr>
                    </div>
                    <div>
                        <div class="uk-form-label uk-animation-slide-bottom">Start time</div>
                        <flatpickr class="uk-input uk-form-large" :conf="{enableTime: true,time_24hr:true, noCalendar:true}"
                                currentdate="{{$article->start or ''}}" inputname="start"></flatpickr>
                    </div>
                    <div>
                        <div class="uk-form-label uk-animation-slide-bottom">Finish time</div>
                        <flatpickr class="uk-input uk-form-large" :conf="{enableTime: true,time_24hr:true, noCalendar:true}"
                                currentdate="{{$article->end or ''}}" inputname="end"></flatpickr>
                    </div>
            </div>
            <div class="uk-child-width-1-2@s" uk-grid>
                    <div>
                        <div class="uk-form-label uk-animation-slide-bottom">Venue</div>
                        <input type="text" class="uk-input uk-form-large" name="venue" value="{{$article->venue or ''}}" placeholder="Venue">
                    </div>
                    <div>
                        <div class="uk-form-label uk-animation-slide-bottom">Event name</div>
                        <localized-input input-name="event_name" 
                        class-name="uk-input uk-form-large" 
                        placeholder="Event name"
                        available-data="{{$article->event_name ? json_encode($article->event_name) : null}}">
                        </localized-input>
                    </div>
            </div>
        </li>
    </ul>