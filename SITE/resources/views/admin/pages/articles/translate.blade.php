@extends('admin.layouts.base')
@section('title', '::New article')
{{--@section('css')--}}
@section('js')
    @parent
    <script type="text/javascript" src="{{asset('plugins/moxiemanager/js/moxman.loader.min.js')}}"></script>
@endsection
@section('content')
    <div class="uk-section">
        <div class="uk-container uk-container-small head-font">
            <form action="{{route('articles.store')}}" method="post">
                {{ csrf_field() }}
                <ul class="uk-subnav uk-subnav-pill" uk-switcher="animation: uk-animation-slide-bottom-medium">
                    <li><a href="#">Title & Cover & Locale</a></li>
                    <li><a href="#">Content</a></li>
                    <li><a href="#">SEO</a></li>
                </ul>
                <ul class="uk-switcher uk-margin">
                    <li>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Name</div>
                            <input type="text" class="uk-input uk-form-large" name="name" placeholder="{{$article->name}}"
                                   value="">
                            <input type="hidden" name="status" value="0">
                            <input type="hidden" name="url" value="{{$article->url}}">
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Short text</div>
                            <textarea name="short" class="uk-textarea uk-form-large" rows="5"
                                      placeholder="{{$article->short}}"></textarea>
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Page</div>
                            <input type="hidden" name="page_id" value="{{$article->page->id}}">
                            <select class="uk-select uk-form-large" disabled>
                                @foreach($pages as $page)
                                    <option value="{{$page->id}}" @if($article->page->id === $page->id) selected @endif>{{$page->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Select locale / <span class="uk-text-medium uk-text-success">Current language:</span> {{$article->locale->name}}</div>
                            <select class="uk-select uk-form-large" name="locale_id">
                                @foreach($locales as $locale)
                                    @if($locale->id != $article->locale->id)
                                        <option value="{{$locale->id}}">{{$locale->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="uk-margin">
                            <imageinput inputname="image" img="{{$article->image}}" btn="Choose image"></imageinput>
                        </div>
                        <div class="uk-margin">
                            <imageinput inputname="cover" img="{{$article->cover}}" btn="Choose cover"></imageinput>
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Cover overlay opacity</div>
                            <range class="uk-form-width-small" name="coveropacity" min="0" max="1" step="0.1"
                                   default="{{$article->coveropacity}}"></range>
                        </div>
                    </li>
                    <li>
                        <medium width="100%" name="content"></medium>
                    </li>
                    <li>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Keywords</div>
                            <input type="text" class="uk-input uk-form-large" name="keywords" value=""
                                   placeholder="{{$article->keywords}}">
                        </div>
                        <div class="uk-margin">
                            <div class="uk-form-label uk-animation-slide-bottom">Description</div>
                            <textarea name="description" class="uk-textarea uk-form-large" rows="5"
                                      placeholder="{{$article->description}}"></textarea>
                        </div>
                    </li>
                </ul>
                <div class="uk-margin">
                    <button class="uk-button uk-button-secondary uk-float-right">Save</button>
                </div>
            </form>
        </div>
    </div>

@endsection