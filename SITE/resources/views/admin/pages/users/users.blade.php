@extends('admin.layouts.base')
@section('title', '::Users')
@section('js')
    @parent
    <script type="text/javascript" src="{{asset('plugins/moxiemanager/js/moxman.loader.min.js')}}"></script>
@endsection
@section('content')
    <div class="uk-section">
        <div class="uk-container uk-container-small uk-margin-bottom">
            <a href="{{route('users.create')}}" class="uk-icon-button uk-button-secondary uk-float-right"
               uk-icon="icon: plus"></a>
        </div>
        <div class="uk-container uk-container-small">
            <table class="uk-table uk-table-responsive uk-table-divider">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)

                    <tr>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->role->name}}</td>
                        <td>
                            <a href="{{route('users.edit', $user->id)}}" class="uk-icon-button uk-button-primary"
                               uk-icon="icon: pencil"></a>
                            @if(Auth::user()->id != $user->id)
                                <a href="{{route('users.delete', $user->id)}}" class="uk-icon-button uk-button-danger"
                                   uk-icon="icon: trash"></a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
