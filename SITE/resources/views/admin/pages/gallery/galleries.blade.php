@extends('admin.layouts.base')
@section('title','::Galleries')
@section('content')
    <div class="uk-section">
        <div class="uk-container uk-container-small uk-margin-bottom">
            <a href="{{route('galleries.create')}}" class="uk-icon-button uk-button-secondary uk-float-right"
               uk-icon="icon: plus"></a>
        </div>
        <div class="uk-container uk-container-small">
            <galleries data="{{$galleries}}"></galleries>
        </div>
    </div>
@endsection