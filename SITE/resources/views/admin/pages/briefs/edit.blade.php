@extends('admin.layouts.base')
@section('title', '::New Reports')
@section('js')
    @parent
    <script type="text/javascript" src="{{asset('plugins/moxiemanager/js/moxman.loader.min.js')}}"></script>
@endsection
@section('content')
    <div class="uk-section">
        <div class="uk-container uk-container-small head-font">
            @include('admin.partials.locale-toggler')
            <form action="{{route('briefs.update', $report->id)}}" method="POST">
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                @include('admin.pages.briefs._form')
                <div class="uk-margin">
                    <a href="{{route('briefs.index')}}" class="uk-button uk-button-danger uk-float-left">Exit without
                        saving</a>
                    <button class="uk-button uk-button-secondary uk-float-right">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection