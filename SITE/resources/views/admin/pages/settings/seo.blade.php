@extends('admin.layouts.base') 
@section('title', '::Site SEO') 
@section('content')
    <div class="uk-section">
        <div class="uk-container uk-container-small">
            <div>
                <h5>SEO</h5>
                <form action="{{route('settings.update','seo')}}" method="POST">
                    {{ method_field('PUT') }}
                    {{ csrf_field() }}
                    <div class="uk-margin uk-grid-small" uk-grid>
                        <div class="uk-with-1-5@s">
                            <locale-toggler></locale-toggler>
                        </div>  
                    </div>
                    <div class="uk-margin">
                        <localized-input
                        class-name="uk-input"
                        input-name="content[title]" 
                        placeholder="Title" 
                        available-data="{{isset($settings->content->title) ? json_encode($settings->content->title) : '{}'}}"></localized-input>
                    </div>
                    <div class="uk-margin">
                        <localized-input
                        class-name="uk-input"
                        input-name="content[keywords]" 
                        placeholder="Keywords " 
                        available-data="{{isset($settings->content->keywords) ? json_encode($settings->content->keywords) : '{}'}}"></localized-input>
                        <small>
                            Between 100–255 characters
                        </small>
                    </div>
                    <div class="uk-margin">
                        <localized-textarea
                        class-name="uk-textarea"
                        input-name="content[description]" 
                        placeholder="Description" 
                        available-data="{{isset($settings->content->description) ? json_encode($settings->content->description) : '{}'}}"></localized-textarea>
                        <small>
                            Between 50–300 characters
                        </small>
                    </div>
                    <div class="uk-margin">
                        <a href="{{route('settings')}}" class="uk-button uk-button-danger uk-float-left">Exit without
                            saving</a>
                        <button class="uk-button uk-button-secondary uk-float-right">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection