@extends('admin.layouts.base')
@section('title','::Settings')
@section('content')
    <div class="uk-section">
        <div class="uk-container uk-container-small">
            <div class="uk-child-width-1-4@s" uk-grid>
                <div>
                    <a href="{{route('settings.for','seo')}}" class="uk-button uk-button-primary uk-width-1-1">Seo</a>
                </div>
                <div>
                    <a href="{{route('settings.for','social')}}" class="uk-button uk-button-primary uk-width-1-1">Social links</a>
                </div>
                <div>
                    <a href="{{route('settings.for','main-slider')}}" class="uk-button uk-button-primary uk-width-1-1">Home page slider</a>
                </div>
                <div>
                    <a href="{{route('settings.for','short-about')}}" class="uk-button uk-button-primary uk-width-1-1">Short about</a>
                </div>
                <div>
                    <a href="{{route('settings.for','contact-form')}}" class="uk-button uk-button-primary uk-width-1-1">Contact form</a>
                </div>
            </div>
        </div>
    </div>
@endsection