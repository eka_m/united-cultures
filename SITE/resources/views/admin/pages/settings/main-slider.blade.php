@extends('admin.layouts.base') 
@section('title', '::Slider') 
@section('js') 
@parent
<script type="text/javascript" src="{{asset('plugins/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/moxiemanager/js/moxman.loader.min.js')}}"></script>
@endsection
 
@section('content')
<div class="uk-section">
    <div class="uk-container uk-container-small">
        <div>
            <h5>Home page slider</h5>
            <form action="{{route('settings.update','main-slider')}}" method="POST">
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                <gallery-images inputname="content" current="{{json_encode($settings->content)}}"></gallery-images>
                <div class="uk-margin">
                    <a href="{{route('settings')}}" class="uk-button uk-button-danger uk-float-left">Exit without
                        saving</a>
                    <button class="uk-button uk-button-secondary uk-float-right">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection