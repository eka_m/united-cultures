@extends('admin.layouts.base') 
@section('title', '::Site Social links') 
@section('content')
    <div class="uk-section">
        <div class="uk-container uk-container-small">
            <div>
                <h5>SEO</h5>
                <form action="{{route('settings.update','social')}}" method="POST">
                    {{ method_field('PUT') }}
                    {{ csrf_field() }}
                    <div class="uk-margin">
                        <label class="uk-label">Facebook</label>
                        <input type="text" name="content[facebook]" class="uk-input" value="{{isset($settings->content->facebook) ? $settings->content->facebook : ''}}" placeholder="Facebook link">
                    </div>
                    <div class="uk-margin">
                        <label class="uk-label">Instagram</label>
                        <input type="text" name="content[instagram]" class="uk-input" value="{{isset($settings->content->instagram) ? $settings->content->instagram : ''}}" placeholder="Instagram link">
                    </div>
                    <div class="uk-margin">
                        <label class="uk-label">Twitter</label>
                        <input type="text"  name="content[twitter]" class="uk-input" value="{{isset($settings->content->twitter) ? $settings->content->twitter : ''}}" placeholder="Twitter link">
                    </div>
                    <div class="uk-margin">
                        <label class="uk-label uk-label-danger">Youtube</label>
                        <input type="text"  name="content[youtube]" class="uk-input" value="{{isset($settings->content->youtube) ? $settings->content->youtube : ''}}" placeholder="Youtube link">
                    </div>
                    <div class="uk-margin">
                        <label class="uk-label uk-label-danger">Google</label>
                        <input type="text"  name="content[google]" class="uk-input" value="{{isset($settings->content->google) ? $settings->content->google : ''}}" placeholder="Google link">
                    </div>
                    <div class="uk-margin">
                        <a href="{{route('settings')}}" class="uk-button uk-button-danger uk-float-left">Exit without
                            saving</a>
                        <button class="uk-button uk-button-secondary uk-float-right">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection