@extends('admin.layouts.base') 
@section('title', '::Contact form') 
@section('content')
    <div class="uk-section">
        <div class="uk-container uk-container-small">
            <div>
                <h5>Contact form</h5>
                <form action="{{route('settings.update','contact-form')}}" method="POST">
                    {{ method_field('PUT') }}
                    {{ csrf_field() }}
                    <div class="uk-margin uk-grid-small" uk-grid>
                        <div class="uk-with-1-5@s">
                            <locale-toggler></locale-toggler>
                        </div>  
                    </div>
                    <div class="uk-margin">
                        <localized-input
                        class-name="uk-input"
                        input-name="content[title]" 
                        placeholder="Form title" 
                        available-data="{{isset($settings->content->title) ? json_encode($settings->content->title) : '{}'}}"></localized-input>
                    </div>
                    <div class="uk-margin">
                        <localized-input
                        class-name="uk-input"
                        input-name="content[success]" 
                        placeholder="Form success message" 
                        available-data="{{isset($settings->content->success) ? json_encode($settings->content->success) : '{}'}}"></localized-input>
                    </div>
                    <div class="uk-margin">
                        <textarea name="content[receivers]" cols="30" rows="10" class="uk-textarea" placeholder="Receivers exmpl: test@mail.com, test2@mail.com">{{isset($settings->content->receivers) ? $settings->content->receivers : ''}}</textarea>
                    </div>
                    <div class="uk-margin">
                        <a href="{{route('settings')}}" class="uk-button uk-button-danger uk-float-left">Exit without
                            saving</a>
                        <button class="uk-button uk-button-secondary uk-float-right">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection