@extends('admin.layouts.base') 
@section('title', '::About in home page') 
@section('js') 
@parent
<script type="text/javascript" src="{{asset('plugins/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/moxiemanager/js/moxman.loader.min.js')}}"></script>
@endsection
 
@section('content')
<div class="uk-section">
    <div class="uk-container uk-container-small">
        <div>
            <h5>Short about</h5>
            <form action="{{route('settings.update','short-about')}}" method="POST">
                {{ method_field('PUT') }} {{ csrf_field() }}
                <div class="uk-margin uk-grid-small" uk-grid>
                    <div class="uk-with-1-5@s">
                        <locale-toggler></locale-toggler>
                    </div>
                </div>
                <div class="uk-margin">
                    <localized-input class-name="uk-input" input-name="content[title]" placeholder="Title" available-data="{{isset($settings->content->title) ? json_encode($settings->content->title) : '{}'}}"></localized-input>
                </div>
                <div class="uk-margin">
                    <input type="text" class="uk-input" placeholder="Readmore link" name="content[link]" value="{{$settings->content->link or ''}}">
                </div>
                <div class="uk-margin">
                    <localized-textarea input-name="content[short]" placeholder="Short text" editor="true" :editor-options='{
                        "height": 200,
                        "autoresize_min_height": 200,
                        "plugins": [
                            "emoticons textcolor colorpicker code paste"
                        ],
                        "toolbar1": "fontsizeselect forecolor backcolor bold italic alignleft aligncenter alignright alignjustify outdent indent code",
                        "fontsize_formats": "8pt 10pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 19pt 20pt 22pt",
                        "branding": false,
                        "menubar": false,
                        "paste_as_text": true,
                    }' available-data="{{isset($settings->content->short) ? json_encode($settings->content->short) : '{}'}}"></localized-textarea>
                </div>
                {{-- <div class="uk-margin">
                    <gallery-images inputname="content[slider]" current="{{isset($settings->content->slider) ? $settings->content->slider : '[]'}}"
                        :show-title="false" :show-pos="false"></gallery-images>
                </div> --}}
                <div class="uk-margin">
                    <a href="{{route('settings')}}" class="uk-button uk-button-danger uk-float-left">Exit without
                        saving</a>
                    <button class="uk-button uk-button-secondary uk-float-right">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection