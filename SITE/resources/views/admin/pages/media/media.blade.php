@extends('admin.layouts.base')
@section('title', '::Mediamanager')
@section('js')
    @parent
    <script type="text/javascript" src="{{asset('plugins/moxiemanager/js/moxman.loader.min.js')}}"></script>
@endsection
@section('content')
    <div class="uk-section">
        <div class="uk-container uk-container-small">
        <mediamanager></mediamanager>
        </div>
    </div>
@endsection
