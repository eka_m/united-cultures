@extends('admin.layouts.base')
@section('title','::Pages')
@section('content')
    <div class="uk-section">
        <div class="uk-container uk-container-small uk-margin-bottom">
            <a href="{{route('pages.create')}}" class="uk-icon-button uk-button-secondary uk-float-right" uk-icon="icon: plus"></a>
        </div>
        <div class="uk-container uk-container-small">
            <tree :pages="{{json_encode($pages)}}" url="pages"></tree>
        </div>
    </div>
@endsection