<ul class="uk-switcher uk-margin">
        <li>
            <div class="uk-margin">
                <div class="uk-form-label uk-animation-slide-bottom">Name</div>
                <localized-input input-name="name" 
                class-name="uk-input uk-form-large" 
                id="translit-it" 
                placeholder="Page name"
                available-data="{{$page->name ? json_encode($page->name) : null}}">
                </localized-input>
            </div>
            <div class="uk-margin">
                <div class="uk-form-label uk-animation-slide-bottom">Slug</div>
                <slug inputname="url" oldval="{{old('url', $page->url)}}"></slug>
            </div>
            <div class="uk-margin">
                <div class="uk-form-label uk-animation-slide-bottom">Placement</div>
                <select class="uk-select uk-form-large" name="place">
                        <option value="mainmenu" @if($page->place == 'mainmenu') selected @endif>Main menu</option>
                        <option value="category" @if($page->place == 'category') selected @endif>Category</option>
                        <option value="both" @if($page->place == 'both') selected @endif>Main menu & Category</option>
                        <option value="link" @if($page->place == 'link') selected @endif>Link</option>
                </select>
            </div>
            <div class="uk-margin">
                <div class="uk-form-label uk-animation-slide-bottom">Type</div>
                <select class="uk-select uk-form-large" name="type">
                        <option value="regular" @if($page->type == 'regular') selected @endif>Regular</option>
                        <option value="route" @if($page->type == 'route') selected @endif>Route</option>
                        <option value="custom" @if($page->type == 'custom') selected @endif>Custom</option>
                </select>
            </div>
            <div class="uk-margin">
                <div class="uk-form-label uk-animation-slide-bottom">Parent</div>
                <select class="uk-select uk-form-large" name="parent_id">
                    <option value="0">No parent</option>
                    @foreach($pages as $item)
                        @continue($page->id === $item->id)
                        <option value="{{$item->id}}"
                                @if($page->parent && $page->parent->id === $item->id) selected @endif>
                                @foreach($locales as $locale)
                               {{$item->name[$locale->slug] or "No translation ($locale->slug)"}} /
                                @endforeach
                            </option>
                    @endforeach
                </select>
            </div>
            <div class="uk-margin">
                <imageinput inputname="cover" btn="Choose Cover"  img="{{old('cover', $page->cover)}}" ></imageinput>
            </div>
            <div class="uk-margin">
                <imageinput inputname="logo" btn="Choose Logo"  img="{{old('cover', $page->logo)}}" ></imageinput>
            </div>
            <div class="uk-margin">
                <div class="uk-form-label uk-animation-slide-bottom">Cover overlay opacity</div>
                <range class="uk-form-width-small" name="coveropacity" min="0" max="1" step="0.1" default="0.5" default="{{old('coveropacity', $page->coveropacity)}}"></range>
            </div>
        </li>
        <li>
            <localized-textarea 
            input-name="content" 
            class-name="uk-textarea uk-form-large" 
            editor="true" 
            available-data="{{$page->content ? json_encode($page->content) : null}}">
            </localized-textarea>
        </li>
        <li>
            <div class="uk-margin">
                <div class="uk-form-label uk-animation-slide-bottom">Keywords</div>
                <localized-input input-name="keywords"
                    class-name="uk-input uk-form-large" 
                    placeholder="Keywords"
                    available-data="{{$page->keywords ? json_encode($page->keywords) : null}}">
                </localized-input>
            </div>
            <div class="uk-margin">
                <div class="uk-form-label uk-animation-slide-bottom">Description</div>
                <localized-textarea input-name="description" 
                class-name="uk-textarea uk-form-large" 
                placeholder="Description"
                available-data="{{$page->description ? json_encode($page->description) : null}}"
                localized="{{json_encode($locales)}}">
                </localized-textarea>
            </div>
        </li>
    </ul>