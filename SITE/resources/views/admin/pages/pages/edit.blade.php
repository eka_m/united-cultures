@extends('admin.layouts.base')
@section('title', '::New Page')
@section('js')
    @parent
    <script type="text/javascript" src="{{asset('plugins/moxiemanager/js/moxman.loader.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('plugins/tinymce/tinymce.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('plugins/tinymce/init.js')}}"></script>
@endsection
@section('content')
    <div class="uk-section">
        <div class="uk-container head-font">
            @include('admin.partials.locale-toggler')
            <form action="{{route('pages.update', $page->id)}}" method="POST">
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                <ul class="uk-subnav uk-subnav-pill" uk-switcher="animation: uk-animation-slide-bottom-medium">
                    <li><a href="#">Title & Cover</a></li>
                    <li><a href="#">Content</a></li>
                    <li><a href="#">SEO</a></li>
                </ul>
                @include('admin.pages.pages._form')
                <div class="uk-margin">
                    <a href="{{route('pages.index')}}" class="uk-button uk-button-danger uk-float-left">Exit without
                        saving</a>
                    <button class="uk-button uk-button-secondary uk-float-right">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection