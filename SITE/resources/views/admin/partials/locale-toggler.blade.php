<div class="uk-margin">
    <ul class="uk-subnav uk-subnav-pill">
        <li>
            <locale-toggler available-locales="{{json_encode($locales)}}"></locale-toggler>
        </li>
    </ul>
</div>