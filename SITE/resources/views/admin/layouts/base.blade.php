<!doctype html>
<html lang="{{$locales[0]['slug']}}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="locales" content="{{json_encode($locales)}}">
    <title>Admin Panel @yield('title')</title>
    @section('css')
        <link rel="stylesheet" href="{{asset('/adminpanel/css/app.css')}}">
    @show
<body>

<div id="app" class="wrapper uk-offcanvas-content">
    @include('admin.partials.main-nav')
    @section('content')
    @show
</div>
@section('js')
    <script src="{{asset('adminpanel/js/manifest.js')}}" type="text/javascript"></script>
    <script src="{{asset('adminpanel/js/vendor.js')}}" type="text/javascript"></script>
    <script src="{{asset('adminpanel/js/app.js')}}" type="text/javascript"></script>
    <script src="{{asset('adminpanel/js/custom.js')}}" type="text/javascript"></script>
@show
@include('admin.partials.notifications')
</body>
</html>