@extends('layouts.base')
@section('title', $article->name[$currentlocale])
@section('css')
<link rel="stylesheet" href="{{asset('/css/animates.css')}}">
<link rel="stylesheet" href="{{asset('/plugins/aos/aos.min.css')}}">
@if($gallery)
<link rel='stylesheet' href='{{asset('/plugins/unitegallery/css/unite-gallery.css')}}' type='text/css' />
<link rel='stylesheet' href='{{asset('/plugins/unitegallery/themes/default/ug-theme-default.css')}}' type='text/css' />
@endif
@endsection

@section('js')

@if($gallery)
    <script type='text/javascript' src="{{asset('/plugins/unitegallery/js/unitegallery.min.js')}}"></script>
    @foreach($gallery as $item)
        <script type='text/javascript' src='/plugins/unitegallery/themes/{{$item->type}}/ug-theme-{{$item->type}}.js'></script>
        <script type="text/javascript">
            $("#gallery{{$item->id}}").unitegallery(
                        {!! '{
                        gallery_theme:"'.$item->type.'",
                        tile_enable_textpanel: false,
                        lightbox_type: "compact",
                        }' !!}
                );
        </script>
    @endforeach
@endif
<script src="{{asset('/plugins/aos/aos.min.js')}}"></script>
@endsection

@section('content')
    <section class="row header pt-xs-5 pt-5 mb-5">
        <div class="col-md-3 text-center d-flex align-items-center justify-content-center">
            <div id="logo" class="logo">
                <a href="{{route('home')}}">
                    <img src="{{asset('images/meduza.gif')}}" alt="United Cultures logo" class="img-fluid w-100">
                </a>
                <div class="social">
                        <a href="{{$social->content['facebook']}}"><img src="/images/facebook-icon.svg" alt="facebook-icon" class="facebook-icon"></a>
                        <a href="{{$social->content['instagram']}}"><img src="/images/instagram-icon.svg" alt="instagram-icon" class="instagram-icon"></a>
                </div>
                </div>
        </div>
        <div class="col-md-9">
            <div class="article-header" data-aos="fade-down">
                <div class="article-cover">
                    <img src="{{asset('uploads'.$article->cover)}}" alt="{{$article->name[$currentlocale]}}" class="img-fluid">
                </div>
                 <div class="article-title">
                    <h3>{{$article->name[$currentlocale]}}</h3>
                 </div>
            </div>
        </div>
    </section>

    <section class="row pt-xs-5 pt-2 pl-md-2 pr-md-2 mb-5">
        <div class="col-12">
            <article class="article" data-aos="zoom-out">
                {!! $article->content !!}

                <div class="w-100 clearfix pt-4 mb-2">
                    <a href="{{route('page','our-events')}}" class="btn btn-custom--default float-left">&larr; All</a>
                    <div class="float-right">
                        @if($prev)
                        <a href="{{route('article',$prev->url)}}" class="btn btn-custom--default">&larr; Previous</a>
                        @endif
                        @if($next)
                        <a href="{{route('article',$next->url)}}" class="btn btn-custom--default ">Next &rarr;</a>
                        @endif
                    </div>
                </div>

            </article>
        </div>
    </section>
@endsection