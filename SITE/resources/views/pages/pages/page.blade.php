@extends('layouts.base') 
@section('title', $page->name[$currentlocale]) 
@section('css')
<link rel="stylesheet" href="{{asset('/css/animates.css')}}">
<link rel="stylesheet" href="{{asset('/plugins/aos/aos.min.css')}}">
@if($gallery)
<link rel='stylesheet' href='{{asset('/plugins/unitegallery/css/unite-gallery.css')}}' type='text/css' />
<link rel='stylesheet' href='{{asset('/plugins/unitegallery/themes/default/ug-theme-default.css')}}' type='text/css' /> 
@endif
@endsection
 
@section('js')
@parent
<script src="{{asset('/js/manifest.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/vendor.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/app.js')}}" type="text/javascript"></script>
@if($gallery)
    <script type='text/javascript' src="{{asset('/plugins/unitegallery/js/unitegallery.min.js')}}"></script>
    @foreach($gallery as $item)
        <script type='text/javascript' src='/plugins/unitegallery/themes/{{$item->type}}/ug-theme-{{$item->type}}.js'></script>
        <script type="text/javascript">
            $("#gallery{{$item->id}}").unitegallery(
                        {!! '{
                        gallery_theme:"'.$item->type.'",
                        tile_enable_textpanel: false,
                        lightbox_type: "compact",
                        }' !!}
                );
        </script>
    @endforeach 
@endif
<script src="{{asset('/plugins/aos/aos.min.js')}}"></script>
@endsection
 
@section('content')
    <section class="row header pt-xs-5 pt-5 mb-5">
        <div class="col-md-3 text-center d-flex align-items-center justify-content-center">
        <div id="logo" class="logo">
            <a href="{{route('home')}}">
                @if($page->logo)
                <img src="{{asset('uploads/'.$page->logo)}}" alt="United Cultures logo" class="img-fluid w-100">
                @else
                <img src="{{asset('images/logo.png')}}" alt="United Cultures logo" class="img-fluid">
                @endif
            </a>
            <div class="social">
                    <a href="{{$social->content['facebook']}}"><img src="/images/facebook-icon.svg" alt="facebook-icon" class="facebook-icon"></a>
                    <a href="{{$social->content['instagram']}}"><img src="/images/instagram-icon.svg" alt="instagram-icon" class="instagram-icon"></a>
            </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="article-header" data-aos="fade-down">
                <div class="article-cover">
                    <img src="{{asset('uploads'.$page->cover)}}" alt="{{$page->name[$currentlocale]}}" class="img-fluid">
                </div>
                 {{-- <div class="article-title">
                    <h3>{{$page->name[$currentlocale]}}</h3>
                 </div> --}}
            </div>
        </div>
    </section>
    @if($page->content)
    <section class="row pt-xs-5 pt-2 pl-md-2 pr-md-2 mb-5">
        <div class="col-12">
            <article class="article" data-aos="zoom-out">
                {!! $page->content !!}
            </article>
        </div>
    </section>
    @endif
    @if(!$articles->isEmpty())
    <section class="row pt-xs-5 pt-2 pl-md-2 pr-md-2 mb-5 section-latest-articles">
        <div class="col-12">
            <div class="row latest-articles">
                @foreach($articles as $article)
                    <article class="col-lg-4 col-md-6 col-sm-6 col-12 slide-item animated pulse">
                        <div class="latest-articles__item">
                        <div class="latest-articles__item__image">
                                <img src="{{asset('/uploads'.$article->image)}}" alt="{{$article->name[$currentlocale]}}" class="img-fluid">
                                <div class="latest-articles__item__readmore">
                                        <a href="{{route('article', $article->url)}}">Readmore</a>
                                </div>
                            </div>
                            <div class="latest-articles__item__info">
                                <h3>{{$article->name[$currentlocale]}}</h3>
                            </div>
                        </div>
                    </article>
                @endforeach
            </div>
            <div class="row">
                    <div class="col p-3 d-flex align-items-center">
                        <div class="d-inline-block m-auto">
                            {{$articles->links('vendor.pagination.bootstrap-4')}}
                        </div>
                    </div>
                </div>
        </div>
    </section>
    @endif
    <div class="clearfix"><br></div>
@endsection