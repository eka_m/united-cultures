<section class="row section-latest-articles">
        <div class="col-12">
            <div class="section-latest-articles__title">
                <h3>Latest <span>Articles</span></h3>
            </div>
            <div class="section-latest-articles__categories">
                <ul>
                    @foreach($categories as $category)
                    <li><a href="" class="active">{{$category->name[$currentlocale]}}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="latest-articles">
                @foreach($articles as $article)
                <div class="slide-item">
                    <article class="latest-articles__item" data-aos="slide-down">
                        <div class="latest-articles__item__image">
                            <img src="{{asset('uploads'.$article->image)}}" alt="" class="img-fluid">
                        </div>
                        <div class="latest-articles__item__info">
                            <h3>{{$article->name[$currentlocale]}}</h3>
                            <div class="latest-articles__item__info__readmore">
                                <a href="#">Readmore</a>
                            </div>
                        </div>
                    </article>
                </div>
                @endforeach
            </div>
        </div>
    </section>