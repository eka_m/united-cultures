@if($nextevent->date)
<section class="row pt-xs-5 pt-5">
    <div class="col-lg-4 col-sm-12 pr-lg-5" data-aos="fade-right" >
        <div class="one-big-event">
            <div class="one-big-event__title">
                <h3>NEXT EVENT</h3>
            </div>
            <div class="one-big-event__info">
                <div class="one-big-event__info__first">
                    @php
                    $day = Carbon\Carbon::createFromFormat('Y-m-d',$nextevent->date)->dayOfWeek != 0 ? Carbon\Carbon::createFromFormat('Y-m-d',$nextevent->date)->dayOfWeek : 7;
                    @endphp
                    <div class="one-big-event__info--day">{{Carbon\Carbon::createFromFormat('Y-m-d',$nextevent->date)->day}}</div>
                    <div class="one-big-event__info__right {{$day == 3 ? 'wednesday' : '' }}">
                        <span>{{__('words.months.'.Carbon\Carbon::createFromFormat('Y-m-d',$nextevent->date)->month)}}</span>
                        <span>{{__('words.week.'.$day)}}</span>
                        <span>{{Carbon\Carbon::createFromFormat('Y-m-d',$nextevent->date)->year}}</span>
                    </div>
                </div>
                <div class="clearfix"></div>
                @if($nextevent->date)
                <div class="one-big-event__info--time">{{$nextevent->start}}</div>
                @endif
               {{--  <div class="one-big-event__info--image">
                    <a href="{{route('article',$nextevent->url)}}">
                         <img src="http://via.placeholder.com/768x500" alt="" class="img-fluid">
                    </a>
                </div> --}}

                <h3 class="mt-3">
                 
                        @if(isset($nextevent->name[$currentlocale]) && !empty(trim($nextevent->name[$currentlocale])) && $nextevent->name[$currentlocale])
                            {{$nextevent->event_name[$currentlocale]}}
                        @else
                            {{$nextevent->name[$currentlocale]}}
                        @endif
                   
                </h3>

            </div>
        </div>
    </div>
    <div class="col-lg-8 col-sm-12 pt-5 hidden-sm-down" data-aos="fade-left">
        <div class="one-big-event--image text-center">
            <a href="{{route('article',$nextevent->url)}}">
                <img src="{{asset('uploads'.$nextevent->image)}}" alt="" class="img-fluid">
            </a>
        </div>
    </div>
</section>
@endif