<div class="section-about">
    <div class="row">
        <div class="col-lg-7 col-md-12 pl-4 section-about__text" data-aos="fade">
            {{--
            <div class="row" data-aos="fade-right">
                <div class="col-12 section-about__title">
                    <span>
                        {{$shortabout->content['title'][$currentlocale]}}
                    </span>
                </div>
            </div>
            --}}
            <div class="row">
                <div class="col-12 p-0">
                    <img alt="" class="img-fluid" id="categoryImage" src="{{asset('uploads/images/Left/foscari_for_website.jpg')}}">
                </div>
            </div>
            {{-- {!! $shortabout->content['short'][$currentlocale] !!} @if($shortabout->content['link'])
            <a class="btn btn-custom btn-custom--default" href="{{$shortabout->content['link']}}">
                READ MORE
            </a>
            @endif --}}
        </div>
        <div class="col-lg-5 col-md-12">
            <div class="row">
                <div class="col-12 text-right pr-4 pt-0 section-about__projtitle">
                    <h4>
                        Our
                        <span>
                            Projects
                        </span>
                    </h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="section-about__link section-about__link--story categoryLink" data-image="/uploads/images/Left/foscari_for_website.jpg" data-aos="flip-down" onclick="window.location = '/page/ca-foscari'">
                        <div class="section-about__link--title">
                            Ca’
                            <span style="display:inline;">
                                Foscari
                            </span>
                        </div>
                        <span class="section-about__link--desc">
                            Ca’ Foscari University of Venice is the first business school in Italy which was founded on 6 August 1868.
                        </span>
                    </div>
                    <div class="section-about__link section-about__link--goals categoryLink" data-image="/uploads/images/sa-back.jpg" data-aos="flip-down" onclick="window.location = '/page/sizinavropa'">
                        <div class="section-about__link--title">
                            Sizin
                            <span>
                                Avropa
                            </span>
                        </div>
                        <span class="section-about__link--desc">
                            SizinAvropa.az is a cultural, educational website aimed to create bridges between EU and Azerbaijan.
                        </span>
                    </div>
                    <div class="section-about__link section-about__link--awards categoryLink" data-image="/uploads/images/uc-back.jpg" data-aos="flip-down" onclick="window.location = '/page/italian-language-course'">
                        <div class="section-about__link--title">
                            124
                            <span>
                                Studios
                            </span>
                        </div>
                        <span class="section-about__link--desc">

                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row pt-3">
        <div class="col-12 pl-2">
            <div class="section-about__link section-about__link--travellers" data-aos="flip-down" onclick="window.location = '/page/travellers-club'">
                <div class="section-about__link--title">
                    Travellers
                    <span>
                         Club
                    </span>
                </div>
                <span class="section-about__link--desc">
                    The Travellers Club Baku is a network of "travellers" who want to share experiences, memories, pictures, flavours of travels to neighbour and distant countries...
                </span>
            </div>
        </div>
    </div>
</div>