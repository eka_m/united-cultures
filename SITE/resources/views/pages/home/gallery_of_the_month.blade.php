<div class="gallery-of-the-month" data-aos="fade-up">
    <div class="gallery-of-the-month__title">
        <h3>
            <span>Gallery</span> of the
            <span>month</span>
        </h3>
    </div>
    <div class="gallery-of-the-month__gallery mt-3">
        <div id="monthgallery" style="display:none;">
            @foreach(json_decode($galleryOfTheMonth->images,true) as $image)
            <img src="{{asset('uploads'.$image['path'])}}" data-image="{{asset('uploads'.$image['path'])}}" alt="{{$image['title'][$currentlocale] or ''}}"
                data-description="{{$image['description'][$currentlocale] or ''}}" class="img-fluid"> @endforeach
        </div>
    </div>
</div>