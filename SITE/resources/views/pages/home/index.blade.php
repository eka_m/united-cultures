@extends('layouts.base') 
@section('title', 'Home page') 
@section('css')
<link rel="stylesheet" href="{{asset('/css/animates.css')}}">
<link rel="stylesheet" href="{{asset('/plugins/aos/aos.min.css')}}">
<link rel="stylesheet" href="{{asset('/plugins/slick/slick.min.css')}}">
<link rel="stylesheet" href="{{asset('/plugins/unitegallery/css/unite-gallery.css')}}">
<link rel="stylesheet" href="{{asset('/plugins/plyr/plyr.min.css')}}">
@endsection
 
@section('js')
<script src="{{asset('/plugins/aos/aos.min.js')}}"></script>
<script src="{{asset('/js/manifest.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/vendor.js')}}" type="text/javascript"></script>
<script src="{{asset('/js/app.js')}}" type="text/javascript"></script>
<script src="{{asset('/plugins/slick/slick.min.js')}}"></script>
<script src="{{asset('/plugins/slick/slick-animation.min.js')}}"></script>
<script src="{{asset('/plugins/slick/slick-init.js')}}"></script>
<script src="{{asset('/plugins/unitegallery/js/unitegallery.min.js')}}"></script>
<script src="{{asset('/plugins/unitegallery/themes/default/ug-theme-default.js')}}"></script>
<script src="{{asset('/plugins/plyr/plyr.min.js')}}"></script>

@endsection
 
@section('content')
<section class="row header p-0">
    <video width="100%" height="100%" autoplay loop poster="{{asset('uploads/media/video-poster.jpg')}}">
        <source src="{{asset('uploads/media/video.mp4')}}" type="video/mp4">
        Your browser does not support the video tag.
      </video>
    {{--<video width="100%" height="100%" autoplay loop poster="{{asset('uploads/media/video-poster.jpg')}}">--}}
        {{--<source src="{{asset('uploads/media/video.webm')}}" type="video/webm">--}}
        {{--<source src="{{asset('uploads/media/video.ogg')}}" type="video/ogg">--}}
        {{--Your browser does not support the video tag.--}}
    {{--</video>--}}
</section>
{{-- <section class="row header pt-xs-5 pt-5 mb-5">
    <div class="col-md-3 text-center d-flex align-items-center justify-content-center">
    @include('partials.logo')
    </div>
    <div class="col-md-9">
    @include('partials.header')
    </div>
</section> --}}

<section class="row pt-xs-5 pt-5 mb-5">
    <div class="col-lg-8 col-md-7 col-sm-12">
    @include('pages.home.short_about')
    @include('pages.home.gallery_of_the_month')
    </div>
    <div class="col-lg-4 col-md-5 col-sm-12 pl-lg-5">
    @include('pages.home.recommended_events')
    </div>
</section>
    {{-- @include('pages.home.latest_articles') --}}
    {{--  <div data-aos="fade-in">
        <articles initial-categories="{{$categories}}" initial-articles="{{$articles}}"></articles>
    </div>  --}}
    @include('pages.home.uc_tv')
    @if($nextevent)
    @include('pages.home.next_event')
    @endif
    @include('pages.home.annual_report')
@endsection