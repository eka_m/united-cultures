<div class="recomended-events border">
    <div class="recomended-events__title">
        <h3>OUR EVENTS</h3>
    </div>
    @foreach($revents as $event)
        <article class="recomended-events__event clearfix">
            <div class="recomended-events__event__inner">
                <img src="{{asset('uploads'.$event->image)}}" alt="" class="img-fluid">
                <div class="recomended-events__event--info" data-aos="zoom-in">
                    @if($event->date)
                    <span>
                        {{__('words.months.'.Carbon\Carbon::createFromFormat('Y-m-d',$event->date)->month)}} 
                        {{Carbon\Carbon::createFromFormat('Y-m-d',$event->date)->day}},
                        {{Carbon\Carbon::createFromFormat('Y-m-d',$event->date)->year}}
                    </span>
                    @endif
                    <h3>{{$event->name[$currentlocale]}}</h3>
                    <p>{{$event->short[$currentlocale]}}</p>
                    <a href="{{route('article',$event->url)}}">+</a>
                    <div class="clearfix"></div>
                </div>
            </div>
        </article>
    @endforeach

    <div class="recomended-events__more-events">
         <a href="/page/our-events" class="link-reset">MORE EVENTS</a>
    </div>
</div>