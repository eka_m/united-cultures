<section class="row pt-xs-5 pb-5 mb-5 annual h-100" data-aos="fade-in">
  <div class="col-lg-4 col-sm-12 pr-lg-5 pb-5">
    <div class="annual__left annual__left--light">
      {{--  <div class="annual__left__title">
          <h3>Annual report</h3>
      </div>  --}}
      <div class="annual__left__image">
        <img src="{{asset('uploads'.$report->image)}}" alt="" class="img-fluid">
      </div>
      @if($profile)
        <div class="annual__left__more">
          <a href="{{asset('uploads'.$profile->file)}}" target="_blank">Company profile
            @svg('images/download-symbol.svg', 'annual__left__more--dicon')
          </a>
        </div>
      @endif
      @if($report)
        <div class="annual__left__more pt-3">
          <a href="{{asset('uploads'.$report->file)}}" target="_blank">Brochure
            @svg('images/download-symbol.svg', 'annual__left__more--dicon')
          </a>
        </div>
      @endif
    </div>
  </div>
  <div class="col-lg-8 col-sm-12 pt-5">
    <div class="annual__right">
      <div class="annual__right__text mt-5 pt-5">
        <p>
          Cultural Diplomacy may best be described as a course of actions, which are based on and utilize the exchange
          of ideas, values,
          traditions and other aspects of culture or identity, in order to strengthen relationships, enhance
          socio-cultural
          cooperation, promote national interests and beyond. </p>
        <p>Cultural diplomacy can be practiced by either the public sector, private sector or civil society.</p>
      </div>
      <div class="annual__right__contacts">
        <div class="annual__right__contacts--title">
          Contacts
        </div>
        <div class="annual__right__contacts--address">
          <img src="/images/location.svg" alt="">
          <span> 96 E Nizami Street, The Landmark, Baku AZ1010, Azerbaijan </span>
        </div>
        <div class="annual__right__contacts--email">
          <img src="/images/at.svg" alt="">
          <span>info@united-cultures.az</span>
        </div>
      </div>
    </div>
  </div>
</section>