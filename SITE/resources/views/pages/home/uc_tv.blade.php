@if(!$videos->isEmpty())
<div class="uc-tv mb-5" data-aos="fade-up">
    <div class="uc-tv__title">
        <h3 class="m-0">
            <span class="uc">UC</span>
            <span class="tv">TV</span>
        </h3>
    </div>
    <div class="uc-tv__gallery">
        <div class="uc-tv__player">
            <div class="uc-tv__player__container" style="{{$videos->count() == 1 ? 'width:100%;': ''}}">
                <div id="uc-tv-player" class="uc-tv-player-box" data-plyr-provider="{{$videos->first()->type}}" data-plyr-embed-id="{{$videos->first()->link}}"></div>
            </div>
            @if($videos->count() > 1)
            <div class="uc-tv__player__thumbs">
                <a href="javascript:void(0)" class="uc-tv__player__thumbs__nav uc-tv__player__thumbs__nav--up">
                    <img src="{{asset('images/chevron-up.png')}}" >
                </a>
                <a href="javascript:void(0)" class="uc-tv__player__thumbs__nav uc-tv__player__thumbs__nav--down">
                    <img src="{{asset('images/chevron-down.png')}}" >
                </a>
               
                <div class="uc-tv__player__thumbs__container">
                        @foreach($videos as $video) 
                        <div class="uc-tv__player__thumbs__container__thumb" data-type="{{$video->type}}" data-link="{{$video->link}}">
                            <div class="uc-tv__player__thumbs__container__thumb--image">
                                @if($video->type == 'youtube') 
                                <img src="https://img.youtube.com/vi/{{$video->link}}/mqdefault.jpg" alt="" class="img-fluid">
                                @else
                                <img src="https://i.vimeocdn.com/video/{{$video->link}}_320.jpg" alt="" class="img-fluid">
                                @endif
                            </div>
                            @if($video->name)
                            <div class="uc-tv__player__thumbs__container__thumb--title animated fadeOutRight">
                                <h3>{{$video->name}}</h3>
                            </div>
                            @endif
                        </div>
                    @endforeach
                </div>
                
            </div>
            @endif
        </div>
    </div>
</div>
@endif