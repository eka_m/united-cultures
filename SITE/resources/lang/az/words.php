<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    // 'lastnews' => 'SON XƏBƏRLƏR',
    'send' => 'Göndər',
    'subscribe' => 'BİZİM XƏBƏRLƏRƏ ABUNƏ OL',
    'enteremail' => 'Emaili daxil edin',
    'interesting' => "MARAQLI",
    'about' => "HAQQIMIZDA",
    'mostread' => "ƏN ÇOX OXUNAN",
    'links' => "KEÇİDLƏR",
    'copyright' => "Sayt Avropa Birliyinin Azərbaycandakı Nümayəndəliyinin dəstəyi ilə hazırlanıb.",
    'search' => "Nə axtarırıq?",
    'calendar' => 'Aylıq təqvim',
    'noevents' => 'Bu tarixdə heç bir tədbir yoxdur.',
    'week' => [1 => 'Bazar ertəsi', 2 => 'Çərşənbə axşamı', 3 => 'Çərşənbə', 4 => 'Cümə axşamı', 5 => 'Cümə', 6 => 'Şənbə', 7 => 'Bazar'],
    'months' => [1 => 'Yanvar', 2 => 'Fevral', 3 => 'Mart', 4 => 'Aprel', 5 => 'May', 6 => 'İyun', 7 => 'İyul', 8 => 'Avqust', 9 => 'Sentyabr', 10 => 'Oktyabr', 11 => 'Noyabr', 12 => 'Dekabr']

];
