<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'lastnews' => 'LAST NEWS',
    'send' => 'Send',
    'subscribe' => 'SUBSCRİBE TO OUR NEWSLETTER',
    'enteremail' => 'Enter your email',
    'interesting' => "INTERESTING",
    'about' => "ABOUT US",
    'mostread' => "FAVORITES",
    'links' => "LINKS",
    'copyright' => "The site was developed with the support of Delegation of the Europen Union to Azerbaijan.",
    'search' => "Type and hit ENTER",
    'calendar' => 'Monthly calendar',
    'noevents' => 'There are no events on this date.',
    'week' => [1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday', 7 => 'Sunday'],
    'months' => [1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December']
];
